// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REG_EXP_H
#define REG_EXP_H
/// Contains the @ref reg::expression class defintion. @file

#include <vector>

#include <unordered_map>

#include <string>

#include <memory>

namespace reg {
class dfa;
class nfa;
/// Represents [formal regular expressions]
/// (https://en.wikipedia.org/wiki/Regular_expression#Formal_language_theory).
/**
 * One should never need to handle such an object directly, however, much less
 * copy or move it and therefore copy and move constructors are deleted.
 *
 * To work with regular expressions, one should use @ref expression::exptr,
 * which aliases a `shared_ptr` to an actual object and can be copied and moved
 * to one&apos;s heart&apos;s content. To access member functions, one might
 * dereference `exptr`s temporarily or, better yet, use the arrow `->` operator.
 *
 * @see expression::exptr
 */
class expression {
 public:
  /// This is the type used to handle regular expressions.
  /**
   * Every method works on `shared_ptr`s to the actual regular expressions, to
   * help with basic comparisons and to save memory.
   *
   * For example, every [symbol](@ref reg::expression::spawnSymbol)&apos;s (and
   * the [empty string](@ref expression::spawnEmptyString)&apos;s and the
   * [empty set](@ref spawnEmptySet)&apos;s) regular expression is only
   * instantiated once and then pointed to by as many `exptr`s as one likes.
   */
  typedef std::shared_ptr<expression const> exptr;
  static char32_t L, R, K, A, E, N;
  static void reset();
  static exptr const& spawnEmptySet();
  static exptr const& spawnEmptyString();
  static exptr const& spawnSymbol(std::string const& symbol);
  static exptr const& spawnSymbol_(char32_t u32symbol);
  static exptr spawnKleene(exptr const& b, bool optimized = true,
                           bool aggressive = false);
  static exptr spawnConcatenation(exptr const& l, exptr const& r,
                                  bool optimized = true,
                                  bool aggressive = false);
  static exptr spawnAlternation(exptr const& l, exptr const& r,
                                bool optimized = true, bool aggressive = false);
  static exptr spawnFromString(std::string const& re, bool optimized = false,
                               bool aggressive = false);
  static exptr spawnFromString_(std::u32string const& u32re,
                                bool optimized = false,
                                bool aggressive = false);
  /// The different purposes an RE may fulfill.
  /// @see spawnEmptySet
  /// @see spawnEmptyString
  /// @see spawnSymbol
  /// @see spawnKleene
  /// @see spawnConcatenation
  /// @see spawnAlternation
  enum struct operation { empty, symbol, kleene, concatenation, alternation };
  size_t size() const;
  operation getOperation() const;
  /// Returns an NFA accepting the language that this RE describes.
  operator nfa const&() const;
  bool operator==(nfa const& other) const;
  bool operator!=(nfa const& other) const;
  std::string extractSymbol() const;
  char32_t extractSymbol_() const;
  std::u32string to_u32string() const;
  std::string to_string() const;
  std::vector<exptr>::const_iterator begin() const;
  std::vector<exptr>::const_iterator end() const;

 private:
  expression();
  expression(char32_t symbol);
  expression(exptr const& l, exptr const& r, operation op);
  expression(exptr const& b);
  expression(expression& e) = delete;
  expression(expression&& e) = delete;
  expression& operator=(expression& e) = delete;
  expression& operator=(expression& e) const = delete;
  expression& operator=(expression&& e) = delete;
  expression& operator=(expression&& e) const = delete;
  static exptr empty;
  static std::unordered_map<char32_t, exptr> symbols;
  std::vector<exptr> const subExpressions;
  operation const op;
  std::unique_ptr<nfa const> mutable acceptingNfa;
  struct parser;
};
}  // namespace reg
#endif
