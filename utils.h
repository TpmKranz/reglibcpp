// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REG_UTILS_H
#define REG_UTILS_H
/// Contains utility classes, objects and functions used throughout the library.
/// @file
#include <codecvt>

#include <locale>

#include <unordered_set>

#include <functional>

#include <valarray>

namespace reg {

/// Basically Java's List interface's indexOf, but as a non-member function and
/// returning the container's size upon failure.
/**
 * @param  container the container to search through
 * @param  element   the element to find the index of
 * @return           the first `i` with `container.begin()[i]==element` or
 * `container.size()` if none is found
 */
template <class C, class T>
size_t index_of(C const& container, T const& element) {
  static_assert(std::is_same<typename C::value_type, T>::value,
                "C must be a container with T as value_type.");
  return static_cast<size_t>(
      std::distance(container.begin(),
                    std::find(container.begin(), container.end(), element)));
}

inline std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>
    converter;  ///< Converts between UTF-8-encoded and UTF-32-encoded strings.

/// Signals that a state was mentioned that isn&apos;t part of the FA.
struct state_not_found : public std::invalid_argument {
  /// Constructs an exception object with the given message.
  inline state_not_found(char const* what_arg = "Cannot find the given state.")
      : std::invalid_argument(what_arg) {}
  /// @overload
  inline state_not_found(std::string&& what_arg)
      : std::invalid_argument(std::move(what_arg)) {}
  /// Constructs an exception object incorporating the given state name in its
  /// message.
  inline state_not_found(std::string const& state)
      : std::invalid_argument("There is no state named " + state) {}
  /// Constructs an exception object incorporating the given state index in
  /// its message.
  inline state_not_found(size_t state_index)
      : std::invalid_argument("There is no state with index " +
                              std::to_string(state_index)) {}
};

/// Signals that an input symbol was used that the FA doesn&apos;t recognize.
struct symbol_not_found : public std::invalid_argument {
  /// Constructs an exception object with the given message.
  inline symbol_not_found(
      char const* what_arg = "Cannot find the given symbol.")
      : std::invalid_argument(what_arg) {}
  /// @overload
  inline symbol_not_found(std::string&& what_arg)
      : std::invalid_argument(std::move(what_arg)) {}
  /// Constructs an exception object incorporating the given symbol in its
  /// message.
  inline symbol_not_found(char32_t symbol)
      : std::invalid_argument("There is no symbol named " +
                              converter.to_bytes(symbol)) {}
  /// Constructs an exception object incorporating the given symbol index in
  /// its message.
  inline symbol_not_found(size_t symbol_index)
      : std::invalid_argument("There is no symbol with index " +
                              std::to_string(symbol_index)) {}
};
}  // namespace reg

namespace std {
template <>
struct hash<valarray<bool>> {
  inline size_t operator()(valarray<bool> const& value) const {
    size_t hash = 0;
    for (bool v : value) {
      hash <<= 1;
      hash |= v;
    }
    return hash;
  }
};

template <>
struct equal_to<valarray<bool>> {
  inline bool operator()(valarray<bool> const& lhs,
                         valarray<bool> const& rhs) const {
    return (lhs.size() == rhs.size() && (lhs == rhs).min() == true);
  }
};
}  // namespace std

#endif
