// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

/// Contains the @ref reg::dfa member definitions. @file
#include "dfa.h"
using std::make_unique;
using std::string;
using std::to_string;
using std::u32string;
using std::valarray;
using std::vector;

using std::move;

#include <unordered_set>
using std::unordered_set;

#include <unordered_map>
using std::unordered_map;

#include <forward_list>
using std::forward_list;

#include <algorithm>
using std::find;
using std::none_of;

#include <cstdint>

#include "fabuilder.h"
#include "nfa.h"
#include "utils.h"

namespace reg {

/// Private implementation details of DFAs.
struct dfa::impl {
  std::shared_ptr<nfa const> mutable equivalent;  ///< Holds an equivalent NFA
                                                  ///< in case it is ever
                                                  ///< needed.
  valarray<bool> accepting;  ///< A `true` value marks an index as belonging to
                             ///< an accept state.
  vector<char32_t> u32alphabet;  ///< Represents the set of processable symbols.
  vector<string> alphabet;  ///< Represents the set of processable symbols as
                            ///< UTF-8-encoded strings.
  vector<string> labels;    ///< Stores the names of states.
  vector<vector<size_t>>
      transitions;  ///< Stores the transition function as a table viz _state
                    ///< index &times; symbol index &rarr; state index_.

  /// Constructs private implementation object for a DFA accepting the empty
  /// language &empty;.
  impl()
      : accepting(false, 1),
        u32alphabet(0),
        alphabet(0),
        labels(1),
        transitions(1) {}

  /// Constructs private implementation object with provided members.
  impl(vector<char32_t>&& u32alphabet, vector<vector<size_t>>&& transitions,
       vector<string>&& labels, valarray<bool>&& accepting)
      : accepting(move(accepting)),
        u32alphabet(move(u32alphabet)),
        labels(move(labels)),
        transitions(move(transitions)) {
    alphabet.reserve(this->u32alphabet.size());
    for (char32_t symbol : this->u32alphabet) {
      alphabet.push_back(converter.to_bytes(symbol));
    }
  }
};

/// Constructs a DFA accepting the empty language &empty;.
dfa::dfa() : pim(new impl) {}

/// Constructs a DFA from a builder by calling its build method.
dfa::dfa(fabuilder& b, bool force) : dfa(b.buildDfa(force)) {}

/// Constructs DFA with a [private implementation object](@ref impl) with
/// provided members.
dfa::dfa(vector<char32_t>&& alphabet, vector<vector<size_t>>&& transitions,
         vector<string>&& labels, valarray<bool>&& acceptingStates)
    : pim(new impl(move(alphabet), move(transitions), move(labels),
                   move(acceptingStates))) {}

/// Copy-constructs a DFA by copying another one&apos;s
/// [private implementation object](@ref impl).
dfa::dfa(dfa const& d) : pim(new impl(*(d.pim))) {}

/// Move-constructs a DFA by stealing another one&apos;s
/// [private implementation object](@ref impl).
/** The other DFA will be accepting the empty language &empty; afterwards. */
dfa::dfa(dfa&& d) : pim(new impl) { pim.swap(d.pim); }

/// Copy-assigns this DFA by copying another one&apos;s
/// [private implementation object](@ref impl).
dfa& dfa::operator=(dfa const& d) {
  if (this != &d) {
    pim.reset(new impl(*(d.pim)));
  }
  return *this;
}

/// Move-assigns this DFA by stealing another one&apos;s
/// [private implementation object](@ref impl).
/** The other DFA will be accepting the empty language &empty; afterwards. */
dfa& dfa::operator=(dfa&& d) {
  if (this != &d) {
    pim.reset(new impl);
    pim.swap(d.pim);
  }
  return *this;
}

dfa::~dfa() = default;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
dfa::operator nfa const&() const {
  if (!pim->equivalent) {
    pim->equivalent = std::make_shared<nfa const>(fabuilder(*this).buildNfa());
  }
  return *pim->equivalent;
}
#endif  // DOXYGEN_SHOULD_SKIP_THIS

/// Tests whether this DFA accepts exactly the same language as another one.
/**
 * More specifically, checks whether this DFA&apos;s initial state is
 * [indistinguishable](@ref indistinguishableStates) from the other one&apos;s.
 */
bool dfa::operator==(dfa const& d) const {
  forward_list<char32_t> unitedAlphabet(getAlphabet_().begin(),
                                        getAlphabet_().end());
  size_t unitedAlphabetSize(getAlphabet_().size());
  for (char32_t symbol : d.getAlphabet_()) {
    if (index_of(getAlphabet_(), symbol) == getAlphabet_().size()) {
      unitedAlphabet.push_front(symbol);
      unitedAlphabetSize++;
    }
  }
  vector<vector<size_t>> tTable(getStates().size() + d.getStates().size() + 1,
                                vector<size_t>(unitedAlphabetSize));
  valarray<bool> accepting(false,
                           getStates().size() + d.getStates().size() + 1);
  for (size_t q(0); q < getStates().size(); q++) {
    size_t s(0);
    vector<size_t>& row = tTable[q];
    for (char32_t symbol : unitedAlphabet) {
      try {
        row[s] = delta_(q, symbol);
      } catch (symbol_not_found e) {
        row[s] = getStates().size() + d.getStates().size();
      }
      s++;
    }
    accepting[q] = isAccepting(q);
  }
  for (size_t q(0); q < d.getStates().size(); q++) {
    size_t s(0);
    vector<size_t>& row = tTable[q + getStates().size()];
    for (char32_t symbol : unitedAlphabet) {
      try {
        row[s] = getStates().size() + d.delta_(q, symbol);
      } catch (symbol_not_found e) {
        row[s] = getStates().size() + d.getStates().size();
      }
      s++;
    }
    accepting[q + getStates().size()] = d.isAccepting(q);
  }
  for (size_t s(0); s < unitedAlphabetSize; s++) {
    tTable[getStates().size() + d.getStates().size()][s] =
        getStates().size() + d.getStates().size();
  }
  return indistinguishableStates(tTable, accepting)[0][getStates().size()];
}

/// Tests whether this DFA doesn&apos;t accept the same language as another one.
bool dfa::operator!=(dfa const& d) const { return !operator==(d); }

/// Tests whether this DFA accepts exactly the same language as another object.
/// @overload
bool dfa::operator==(nfa const& n) const {
  return operator==(static_cast<dfa const&>(n));
}

/// Tests whether this DFA doesn&apos;t accept the same language as another
/// object. @overload
bool dfa::operator!=(nfa const& n) const { return !operator==(n); }

/// Computes this DFA&apos;s transition function for a state index and a symbol
/// index.
/**
 * @param  qi      the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @param  si index of the symbol to process (&lt; @ref getAlphabet()`.size()`)
 * @return        the reached state&apos;s index
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if @p si &ge; @ref getAlphabet()`.size()`
 */
size_t dfa::delta(size_t qi, size_t si) const {
  if (si >= pim->alphabet.size()) {
    throw symbol_not_found(si);
  }
  if (qi >= pim->labels.size()) {
    throw state_not_found(qi);
  }
  return pim->transitions[qi][si];
}

/// Computes this DFA&apos;s transition function for a state index and a
/// UTF-32-encoded symbol.
/**
 * This looks up the @ref index_of @p u32symbol within @ref getAlphabet_() and
 * calls @ref delta(size_t,size_t) const.
 * If you don&apos;t want that overhead and already have a symbol index on your
 * hands, use that method.
 *
 * @param  qi        the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @param  u32symbol the symbol to process (&isin; @ref getAlphabet_())
 * @return           the reached state&apos;s index
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if @p u32symbol &notin; @ref getAlphabet_()
 */
size_t dfa::delta_(size_t qi, char32_t u32symbol) const {
  try {
    return delta(qi, index_of(getAlphabet_(), u32symbol));
  } catch (symbol_not_found e) {
    throw symbol_not_found(u32symbol);
  }
}

/// Computes this DFA&apos;s transition function for a state index and a
/// UTF-8-encoded symbol.
/**
 * This converts @p symbol to UTF-32 and calls
 * @ref delta_(size_t,char32_t) const with the @ref std::u32string&apos;s first
 * `char32_t`.
 * If you don&apos;t want that overhead and already have a `char32_t` on your
 * hands, use that method.
 *
 * @param  qi     the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @param  symbol the symbol to process (&isin; @ref getAlphabet())
 * @return        the reached state&apos;s index
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if @p symbol &notin; @ref getAlphabet()
 */
size_t dfa::delta(size_t qi, string const& symbol) const {
  return delta_(qi, converter.from_bytes(symbol)[0]);
}

/// Computes this DFA&apos;s transition function for a state label and a
/// UTF-32-encoded symbol.
/**
 * This looks up the @ref index_of @p q within @ref getStates() and calls
 * @ref delta_(size_t,char32_t) const.
 * If you don&apos;t want that overhead and already have a state index on your
 * hands, use that method.
 *
 * @param  q         the state&apos;s label (&isin; @ref getStates())
 * @param  u32symbol the symbol to process (&isin; @ref getAlphabet_())
 * @return           the reached state&apos;s label
 * @throws state_not_found if @p q &notin; @ref getStates()
 * @throws symbol_not_found if @p u32symbol &notin; @ref getAlphabet_()
 */
string const& dfa::delta_(string const& q, char32_t u32symbol) const {
  try {
    return getStates()[delta_(index_of(getStates(), q), u32symbol)];
  } catch (state_not_found e) {
    throw state_not_found(q);
  }
}

/// Computes this DFA&apos;s transition function for a state label and a
/// UTF-8-encoded symbol.
/**
 * This converts @p symbol to UTF-32 and calls
 * @ref delta_(std::string const&,char32_t) const with the
 * @ref std::u32string&apos;s first `char32_t`.
 * If you don&apos;t want that overhead and already have a `char32_t` on your
 * hands, use that method.
 *
 * @param  q      the state&apos;s label (&isin; @ref getStates())
 * @param  symbol the symbol to process (&isin; @ref getAlphabet())
 * @return        the reached state&apos;s label
 * @throws state_not_found if @p q &notin; @ref getStates()
 * @throws symbol_not_found if @p symbol &notin; @ref getAlphabet()
 */
string const& dfa::delta(string const& q, string const& symbol) const {
  return delta_(q, converter.from_bytes(symbol)[0]);
}

/// Computes this DFA&apos;s transition function recursively for a
/// UTF-32-encoded string of symbols, starting in a state specified by its
/// index.
/**
 * @param  qi      the starting state&apos;s index (&lt;
 *                 @ref getStates()`.size()`)
 * @param  u32word the string of symbols to process (all of which &isin;
 *                 @ref getAlphabet_())
 * @return         the reached state&apos;s index
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if one of the @p u32word symbols &notin;
 *                 @ref getAlphabet_()
 */
size_t dfa::deltaHat_(size_t qi, u32string const& u32word) const {
  for (char32_t symbol : u32word) {
    qi = delta_(qi, symbol);
  }
  return qi;
}

/// Computes this DFA&apos;s transition function recursively for a UTF-8-encoded
/// string of symbols, starting in a state specified by its index.
/**
 * This converts @p word to UTF-32 and calls
 * @ref deltaHat_(size_t, std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a
 * @ref std::u32string on your hands, use that method.
 *
 * @param  qi   the starting state&apos;s index (&lt; @ref getStates()`.size()`)
 * @param  word the string of symbols to process (all of which &isin;
 *              @ref getAlphabet())
 * @return      the reached state&apos;s index
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if one of the @p word symbols &notin;
 *              @ref getAlphabet()
 */
size_t dfa::deltaHat(size_t qi, string const& word) const {
  return deltaHat_(qi, converter.from_bytes(word));
}

/// Computes this DFA&apos;s transition function recursively for a
/// UTF-32-encoded string of symbols, starting in a state specified by its name.
/**
 * This looks up the @ref index_of @p q within @ref getStates() and calls
 * @ref deltaHat_(size_t,std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a state index on your
 * hands, use that method.
 *
 * @param  q       the starting state&apos;s label (&isin; @ref getStates())
 * @param  u32word the string of symbols to process (all of which &isin;
 *                 @ref getAlphabet_())
 * @return         the reached state&apos;s index
 * @throws state_not_found if @p q &notin; @ref getStates()
 * @throws symbol_not_found if one of the @p u32word symbols &notin;
 *                 @ref getAlphabet_()
 */
string const& dfa::deltaHat_(string const& q, u32string const& u32word) const {
  return pim->labels[deltaHat_(index_of(getStates(), q), u32word)];
}

/// Computes this DFA&apos;s transition function recursively for a UTF-8-encoded
/// string of symbols, starting in a state specified by its name.
/**
 * This converts @p word to UTF-32 and calls
 * @ref deltaHat_(std::string const&, std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a @ref std::u32string
 * on your hands, use that method.
 *
 * @param  q    the starting state&apos;s label (&isin; @ref getStates())
 * @param  word the string of symbols to process (all of which &isin;
 *              @ref getAlphabet())
 * @return      the reached state&apos;s index
 * @throws state_not_found if @p q &notin; @ref getStates()
 * @throws symbol_not_found if one of the @p word symbols &notin;
 *              @ref getAlphabet()
 */
string const& dfa::deltaHat(string const& q, string const& word) const {
  return deltaHat_(q, converter.from_bytes(word));
}

/// Names this DFA&apos;s initial state.
/**
 * @return   the initial state&apos;s name
 */
string const& dfa::getInitialState() const { return getStates()[0]; }

/// Fetches this DFA&apos;s set of states.
/**
 * @return a vector containing the names of states that can be used as input for
 *         @ref delta(), @ref deltaHat(), @ref delta_(), and @ref deltaHat_()
 */
vector<string> const& dfa::getStates() const { return pim->labels; }

/// Fetches this DFA&apos;s set of processable symbols.
/**
 * @return a vector containing all the valid symbol inputs for @ref delta_() and
 *         @ref deltaHat_()
 */
vector<char32_t> const& dfa::getAlphabet_() const { return pim->u32alphabet; }

/// Fetches this DFA&apos;s set of processable symbols as UTF-8-encoded strings.
/**
 * @return a vector containing all the valid symbol inputs for @ref delta() and
 *         @ref deltaHat()
 */
vector<string> const& dfa::getAlphabet() const { return pim->alphabet; }

/// Tests whether a state is an accept state within this DFA.
/**
 * @param  qi the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @return   `true` if the state is in the set of accept states, `false` else
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 */
bool dfa::isAccepting(size_t qi) const {
  if (qi >= pim->labels.size()) {
    throw state_not_found(qi);
  }
  return pim->accepting[qi];
}

/// Tests whether a state is an accept state within this DFA.
/**
 * @param  q the state&apos;s label (&isin; @ref getStates())
 * @return   `true` if the state is in the set of accept states, `false` else
 * @throws state_not_found if @p q &notin; @ref getStates()
 */
bool dfa::isAccepting(string const& q) const {
  try {
    return isAccepting(index_of(getStates(), q));
  } catch (state_not_found e) {
    throw state_not_found(q);
  }
}

/// Searches the shortest UTF-32-encoded word accepted by a given DFA.
/**
 * @param d the DFA
 * @return the shortest word leading to one of the DFA&apos;s accept states
 * @throws std::logic_error if the DFA doesn&apos;t accept any words
 */
u32string findShortestWord_(dfa const& d) {
  auto const& pim = d.pim;
  if (pim->accepting[0]) {
    return U"";
  }
  unordered_map<size_t, u32string> shortestWords(pim->labels.size());
  size_t oldSize = 0;
  shortestWords.emplace(0, U"");
  while (shortestWords.size() > oldSize) {
    oldSize = shortestWords.size();
    for (auto const& stateWord : shortestWords) {
      for (auto symbol : d.getAlphabet_()) {
        size_t reached = d.delta_(stateWord.first, symbol);
        u32string shWord = stateWord.second + symbol;
        if (pim->accepting[reached]) {
          return shWord;
        }
        if (!shortestWords.count(reached)) {
          shortestWords.emplace(reached, move(shWord));
        }
      }
    }
  }
  throw std::logic_error("This DFA doesn't accept any words!");
}

/// Searches the shortest UTF-8-encoded word accepted by a given DFA.
/**
 * This calls @ref findShortestWord_(dfa const&) and converts the result to
 * UTF-8.
 * If you don&apos;t want that overhead and can handle UTF-32-encoded strings,
 * use that function.
 *
 * @param d the DFA
 * @return the shortest word leading to one of the DFA&apos;s accept states
 * @throws std::logic_error if the DFA doesn&apos;t accept any words
 */
string findShortestWord(dfa const& d) {
  return converter.to_bytes(findShortestWord_(d));
}

/// Creates a builder for a DFA accepting the union of the languages of two
/// DFAs.
/**
 * The input DFAs' state names will be concatenated and collisions resolved by
 * appending `_` in the created DFA.
 *
 * @param d1 the first DFA
 * @param d2 the other DFA
 * @return   builder for a DFA accepting all the words accepted by any of the
 *           input DFAs
 */
fabuilder dfa::unite(dfa const& d1, dfa const& d2) {
  return fabuilder(d1).unite(d2);
}

/// Creates a builder for a DFA accepting the intersection of the languages of
/// two DFAs.
/**
 * The input DFAs' state names will be concatenated and collisions resolved by
 * appending `_` in the created DFA.
 *
 * @param d1 the first DFA
 * @param d2 the other DFA
 * @return   builder for a DFA accepting all the words accepted by both of the
 *           input DFAs
 */
fabuilder dfa::intersect(dfa const& d1, dfa const& d2) {
  return fabuilder(d1).intersect(d2);
}

/// Creates a builder for a DFA accepting the set difference of the languages of
/// two DFAs.
/**
 * The input DFAs' state names will be concatenated and collisions resolved by
 * appending `_` in the created DFA.
 *
 * @param d1 the first DFA
 * @param d2 the other DFA
 * @return   builder for a DFA accepting all the words accepted by the first but
 *           not the other input DFA
 */
fabuilder dfa::subtract(dfa const& d1, dfa const& d2) {
  fabuilder b1(d1);
  for (auto symbol : d2.getAlphabet_()) {
    b1.addSymbol_(symbol);
  }
  return b1.complement().unite(d2).complement();
}

/// Creates a builder for a DFA accepting the complement of the language of a
/// DFA.
/**
 * The input DFAs' state names will be retained in the created DFA.
 *
 * @param d the DFA
 * @return  builder fo a DFA accepting all words not accepted by the input DFA
 *          (provided they can be built from symbols of that DFA&apos;s
 *          [alphabet](@ref getAlphabet()))
 */
fabuilder dfa::complement(dfa const& d) { return fabuilder(d).complement(); }

/// Builds the table of indistinguishable states w.r.t. a transition function.
/**
 * @param transitions the transition function to base indistinguishability
 *                    computation off
 * @param accepting   the set of states that&apos;s trivially distinguishable
 *                    from the rest
 * @return            _state index &times; state index_ table where `true`
 *                    values mark indistinguishable states
 */
vector<valarray<bool>> dfa::indistinguishableStates(
    vector<vector<size_t>> const& transitions,
    valarray<bool> const& accepting) {
  vector<valarray<bool>> distinguishable;
  distinguishable.reserve(transitions.size());
  for (size_t q(0); q < transitions.size(); q++) {
    bool qAcc(accepting[q]);
    distinguishable.push_back(valarray<bool>(false, transitions.size()));
    for (size_t p(0); p < q; p++) {
      bool pAcc(accepting[p]);
      distinguishable[q][p] = (qAcc != pAcc);
      distinguishable[p][q] = (qAcc != pAcc);
    }
  }
  bool changes(true);
  while (changes) {
    changes = false;
    for (size_t q(0); q < transitions.size(); q++) {
      for (size_t p(0); p < q; p++) {
        if (distinguishable[q][p]) {
          continue;
        }
        for (size_t s(0); s < transitions[q].size(); s++) {
          size_t qS(transitions[q][s]);
          size_t pS(transitions[p][s]);
          if (distinguishable[qS][pS]) {
            changes = true;
            distinguishable[q][p] = true;
            distinguishable[p][q] = true;
            break;
          }
        }
      }
    }
  }
  valarray<bool> allTrue(true, transitions.size());
  for (size_t i(0); i < distinguishable.size(); i++) {
    distinguishable[i] ^= allTrue;
  }
  return distinguishable;
}

}  // namespace reg
