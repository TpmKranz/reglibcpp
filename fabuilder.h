// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REG_FABUILDER_H
#define REG_FABUILDER_H
/// Contains the @ref reg::fabuilder class definition. @file

#include "dfa.h"
#include "nfa.h"

namespace reg {
/// Constructs finite automata step by step.
/**
 * Any mention of a symbol or state will add them to the alphabet/set of states.
 * The first state mentioned will be designated initial state.
 */
class fabuilder {
 public:
  fabuilder();
  fabuilder(nfa const& nfa);
  fabuilder(dfa const& dfa);
  fabuilder(fabuilder const& b);
  fabuilder(fabuilder&& b);
  fabuilder& operator=(fabuilder const& b);
  fabuilder& operator=(fabuilder&& b);
  virtual ~fabuilder();

  fabuilder& setAccepting(std::string const& state, bool accept);
  fabuilder& makeInitial(std::string const& state);
  fabuilder& addSymbol(std::string const& symbol);
  fabuilder& addTransition(std::string const& from, std::string const& to,
                           std::string const& symbol = "");
  fabuilder& addSymbol_(char32_t u32symbol);
  fabuilder& addTransition_(std::string const& from, std::string const& to,
                            char32_t u32symbol = U'\0');
  fabuilder& powerset();
  fabuilder& complete();
  fabuilder& merge(bool force = false);
  fabuilder& purge();
  fabuilder& minimize(bool force = false);
  fabuilder& unite(nfa const& other);
  fabuilder& intersect(nfa const& other);
  fabuilder& subtract(nfa const& other, bool force = false);
  fabuilder& complement(bool force = false);
  fabuilder& normalizeStateNames(std::string const& prefix);
  bool isComplete();
  bool hasNondeterminism();
  nfa buildNfa();
  dfa buildDfa(bool force = false);

  /// Signals that an operation requires full determinism and that no powerset
  /// construction was forced.
  struct nondeterminism_exception : public std::logic_error {
    /// Constructs an exception object with the given message.
    inline nondeterminism_exception(
        char const* what_arg = "Builder has nondeterministic characteristics.")
        : std::logic_error(what_arg) {}
    /// @overload
    inline nondeterminism_exception(std::string const& what_arg)
        : std::logic_error(what_arg) {}
  };

 private:
  struct impl;
  std::unique_ptr<impl> pim;
};
}  // namespace reg
#endif  // REG_FABUILDER_H
