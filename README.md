reglibcpp, a C++ implementation of models for regular languages.
====

This library should provide a building block for learning applications or any
other kind of applications incorporating regular language formalisms.

It provides modellings of deterministic finite automata, nondeterministic finite
automata with ε-transitions, generalized nondeterministic finite automata and
formal regular expressions.

These are convertible into each other and can be manipulated w.r.t. the
languages they describe. Set operations like union, intersection, complement and
equality checking are possible on a language level.

# Documentation
- Most recent version
  - https://reglibcpp.tomkra.nz
  - Hosted by [GitLab Pages](https://about.gitlab.com/product/pages/)
- Archive
  - https://cs.uni-potsdam.de/~tokranz/doc
  - Hosted by the [Institute for Computer Science](https://cs.uni-potsdam.de) at
    the [University of Potsdam](https://uni-potsdam.de)

# Source code
- [GNU General Public License version 3](https://www.gnu.org/licenses/) licensed
- obtainable from [this git repo](https://gitlab.com/TpmKranz/reglibcpp/)
