// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

#ifndef REG_GNFA_H
#define REG_GNFA_H
/// Contains the @ref reg::gnfa class definition. @file

#include <memory>

#include <string>

#include <vector>

#include <functional>

#include "expression.h"

namespace reg {
/// Represents [generalized nondeterministic finite automata]
/// (https://en.wikipedia.org/wiki/Generalized_nondeterministic_finite_automaton).
class gnfa {
 public:
  /// Nicer name for arrays of names of states. Should store references to
  /// existing state names.
  typedef std::vector<std::reference_wrapper<std::string const>> state_vector;
  /// Nicer name for arrays of pairs names of states. Should store pairs of
  /// references to existing state names.
  typedef std::vector<std::pair<std::reference_wrapper<std::string const>,
                                std::reference_wrapper<std::string const>>>
      state_pair_vector;
  gnfa(dfa const& d);
  gnfa(nfa const& n);
  gnfa(expression::exptr r);
  gnfa(gnfa const& n);
  gnfa(gnfa&& n);
  gnfa& operator=(gnfa const& n);
  gnfa& operator=(gnfa&& n);
  virtual ~gnfa();

  /// Returns an NFA accepting the same language as this GNFA.
  operator nfa const&() const;
  bool operator==(nfa const& n) const;
  bool operator!=(nfa const& n) const;
  std::string const& getInitialState() const;
  std::string const& getAcceptingState() const;
  state_vector getActiveStates() const;
  expression::exptr getTransition(std::string const& q,
                                  std::string const& p) const;
  state_pair_vector getSplittableTransitions() const;
  state_vector splitTransition(std::string const& q, std::string const& p);
  nfa splitAllTransitions();
  void bypassTransition(
      std::string const& q, std::string const& p,
      std::unordered_map<std::string, expression::exptr> const& offer = {});
  void ripState(std::string const& q,
                std::unordered_map<
                    std::string,
                    std::unordered_map<std::string, expression::exptr>> const&
                    offer = {});
  expression::exptr ripAllStates();

 private:
  friend expression::operator nfa const&() const;
  gnfa(expression const& r);
  struct impl;
  std::unique_ptr<impl> pim;
};
}  // namespace reg
#endif
