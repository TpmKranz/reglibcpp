// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

/// Contains the @ref reg::nfa member definitions. @file
#include "nfa.h"
using std::make_pair;
using std::make_unique;
using std::reference_wrapper;
using std::string;
using std::u32string;
using std::unordered_set;
using std::valarray;
using std::vector;

using std::move;

#include <unordered_map>
using std::unordered_map;

#include <algorithm>
using std::find;
using std::min;

#include <iterator>

#include "dfa.h"
#include "expression.h"
#include "fabuilder.h"

namespace reg {

/// Private implementation details of NFAs.
struct nfa::impl {
  std::shared_ptr<dfa const> mutable equivalent;
  ///< Stores a minimal DFA accepting the same language as this object's NFA.
  valarray<bool> accepting;
  ///< A `true` value marks an index as belonging to an accept state.
  vector<char32_t> u32alphabet;
  ///< Represents the set of processable symbols.
  vector<string> alphabet;
  ///< Represents the set of processable symbols as UTF-8-encoded strings.
  vector<valarray<bool>> mutable epsClosures;
  ///< Cache for every state's &epsilon;-closures.
  vector<string> labels;  ///< Stores the names of states.
  vector<vector<valarray<bool>>> transitions;
  ///< Stores the transition function as a table viz
  ///< _state index &times; symbol index &rarr; list of `bool`s with `true` at
  ///< reached states&apos; indices_.

  /// Constructs private implementation object for an NFA accepting the empty
  /// language &empty;.
  impl()
      : equivalent(),
        accepting(false, 1),
        u32alphabet(1, U'\0'),
        alphabet(1, ""),
        epsClosures(1),
        labels(1),
        transitions(1, vector<valarray<bool>>(1, valarray<bool>(false, 1))) {}

  /// Constructs private implementation object with provided members and empty
  /// [&epsilon;-closure cache](@ref epsClosures).
  impl(vector<char32_t>&& u32alphabet,
       vector<vector<valarray<bool>>>&& transitions, vector<string>&& labels,
       valarray<bool>&& acceptingStates)
      : accepting(move(acceptingStates)),
        u32alphabet(move(u32alphabet)),
        labels(move(labels)),
        transitions(move(transitions)) {
    alphabet.reserve(this->u32alphabet.size());
    for (char32_t symbol : this->u32alphabet) {
      alphabet.push_back(converter.to_bytes(u32string(!!symbol, symbol)));
    }
    epsClosures = vector<valarray<bool>>(this->labels.size());
  }
};

/// Constructs an NFA accepting the empty language &empty;.
nfa::nfa() : pim(new impl) {}

/// Constructs an NFA from a builder by calling its buils
/// method.
nfa::nfa(fabuilder& b) : nfa(b.buildNfa()) {}

/// Constructs NFA with a [private implementation object](@ref impl) with
/// provided members.
nfa::nfa(vector<char32_t>&& alphabet,
         vector<vector<valarray<bool>>>&& transitions, vector<string>&& labels,
         valarray<bool>&& acceptingStates)
    : pim(new impl(move(alphabet), move(transitions), move(labels),
                   move(acceptingStates))) {}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
nfa::operator dfa const&() const {
  if (!pim->equivalent) {
    pim->equivalent = std::make_shared<dfa const>(
        fabuilder(*this).minimize(true).normalizeStateNames("").buildDfa());
  }
  return *pim->equivalent;
}
#endif  // DOXYGEN_SHOULD_SKIP_THIS

/// Checks whether this NFA describes the same regular language as another
/// object.
/**
 * @return `false` if this NFA&apos;s language is exactly the same as the other
 *         object&apos;s, `true` else
 */
bool nfa::operator==(nfa const& n) const {
  return static_cast<dfa const&>(*this).operator==(n);
}

/// Checks whether this NFA describes a different regular language than another
/// object.
/**
 * @return `false` if this NFA&apos;s language is exactly the same as the other
 *         object&apos;s, `true` else
 */
bool nfa::operator!=(nfa const& n) const {
  return static_cast<dfa const&>(*this).operator!=(n);
}

/// Computes this NFA&apos;s transition function for a state index and a symbol
/// index.
/**
 * @param  qi the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @param  si index of the symbol to process (&lt; @ref getAlphabet()`.size()`)
 * @return    valarray with `true` at reached states&apos; indices
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if @p si &ge; @ref getAlphabet()`.size()`
 */
valarray<bool> const& nfa::delta(size_t qi, size_t si) const {
  if (si >= pim->alphabet.size()) {
    throw symbol_not_found(si);
  }
  if (qi >= pim->labels.size()) {
    throw state_not_found(qi);
  }
  return pim->transitions[qi][si];
}

/// Computes this NFA&apos;s transition function for a state index and a
/// UTF-32-encoded symbol.
/**
 * This looks up the @ref index_of @p u32symbol within @ref getAlphabet_() and
 * calls @ref delta(size_t, size_t) const.
 * If you don&apos;t want that overhead and already have a symbol index on your
 * hands, use that method.
 *
 * @param  qi        the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @param  u32symbol the symbol to process (&isin; @ref getAlphabet_())
 * @return           valarray with `true` at reached states&apos; indices
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if @p u32symbol &notin; @ref getAlphabet_()
 */
valarray<bool> const& nfa::delta_(size_t qi, char32_t u32symbol) const {
  try {
    return delta(qi, index_of(getAlphabet_(), u32symbol));
  } catch (symbol_not_found e) {
    throw symbol_not_found(u32symbol);
  }
}

/// Computes this NFA&apos;s transition function for a state index and a
/// UTF-8-encoded symbol.
/**
 * This converts @p symbol to UTF-32 and calls
 * @ref delta_(size_t,char32_t) const with the @ref std::u32string&apos;s first
 * `char32_t`.
 * If you don&apos;t want that overhead and already have a `char32_t` on your
 * hands, use that method.
 *
 * @param  qi     the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @param  symbol the symbol to process (&isin; @ref getAlphabet())
 * @return        valarray with `true` at reached states&apos; indices
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if @p symbol &notin; @ref getAlphabet()
 */
valarray<bool> const& nfa::delta(size_t qi, string const& symbol) const {
  return delta_(qi, converter.from_bytes(symbol)[0]);
}

/// Computes this NFA&apos;s transition function for a state index and a
/// UTF-32-encoded symbol.
/**
 * This looks up the @ref index_of @p q within @ref getStates() and calls
 * @ref delta_(size_t,char32_t) const.
 * If you don&apos;t want that overhead and already have a state index on your
 * hands, use that method.
 *
 * @param  q         the state&apos;s label (&isin; @ref getStates)
 * @param  u32symbol the symbol to process (&isin; @ref getAlphabet_())
 * @return           set of reached states&apos; labels
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if @p u32symbol &notin; @ref getAlphabet_()
 */
nfa::state_set nfa::delta_(string const& q, char32_t u32symbol) const {
  try {
    return decodeSet(delta_(index_of(getStates(), q), u32symbol));
  } catch (state_not_found e) {
    throw state_not_found(q);
  }
}

/// Computes this NFA&apos;s transition function for a state label and a
/// UTF-8-encoded symbol.
/**
 * This converts @p symbol to UTF-32 and calls
 * @ref delta_(std::string const&,char32_t) const with the
 * @ref std::u32string&apos;s first `char32_t`.
 * If you don&apos;t want that overhead and already have a `char32_t` on your
 * hands, use that method.
 *
 * @param  q      the state&apos;s label (&isin; @ref getStates())
 * @param  symbol the symbol to process (&isin; @ref getAlphabet())
 * @return        set of reached states&apos; labels
 * @throws state_not_found if @p q &notin; @ref getStates()
 * @throws symbol_not_found if @p symbol &notin; @ref getAlphabet()
 */
nfa::state_set nfa::delta(string const& q, string const& symbol) const {
  return delta_(q, converter.from_bytes(symbol)[0]);
}

/// Computes this NFA&apos;s transition function recursively for a string of
/// symbols, starting in a state specified by its index.
/**
 * @param  qi      the starting state&apos;s index (&lt;
 *                 @ref getStates()`.size()`)
 * @param  u32word the string of symbols to process (all of which &isin;
 *                 @ref getAlphabet_())
 * @return         valarray with `true` at reached states&apos; indices
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if one the @p u32word symbols &notin;
 *                 @ref getAlphabet_()
 */
valarray<bool> nfa::deltaHat_(size_t qi, u32string const& u32word) const {
  if (qi >= pim->labels.size()) {
    throw state_not_found(qi);
  }
  valarray<bool> qs(pim->labels.size());
  qs[qi] = true;
  return deltaHat_(qs, u32word);
}

/// Computes this DFA&apos;s transition function recursively for a UTF-8-encoded
/// string of symbols, starting in a state specified by its index.
/**
 * This converts @p word to UTF-32 and calls
 * @ref deltaHat_(size_t, std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a @ref std::u32string
 * on your hands, use that method.
 *
 * @param  qi   the starting state&apos;s index (&lt; @ref getStates()`.size()`)
 * @param  word the string of symbols to process (all of which &isin;
 *              @ref getAlphabet())
 * @return      valarray with `true` at reached states&apos; indices
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 * @throws symbol_not_found if one of the @p word symbols &notin;
 *              @ref getAlphabet()
 */
valarray<bool> nfa::deltaHat(size_t qi, string const& word) const {
  return deltaHat_(qi, converter.from_bytes(word));
}

/// Computes this NFA&apos;s transition function recursively for a string of
/// symbols, starting in a state specified by its name.
/**
 * This looks up the @ref index_of @p q within @ref getStates() and calls
 * @ref deltaHat_(size_t,std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a state index on your
 * hands, use that method.
 *
 * @param  q       the starting state&apos;s label (&isin; @ref getStates())
 * @param  u32word the string of symbols to process (all of which &isin;
 *                 @ref getAlphabet_())
 * @return         set of reached states&apos; labels
 * @throws state_not_found if @p q &notin; @ref getStates
 * @throws symbol_not_found if one the @p u32word symbols &notin;
 *                 @ref getAlphabet_()
 */
nfa::state_set nfa::deltaHat_(string const& q, u32string const& u32word) const {
  try {
    return decodeSet(deltaHat_(index_of(getStates(), q), u32word));
  } catch (state_not_found e) {
    throw state_not_found(q);
  }
}

/// Computes this NFA&apos;s transition function recursively for a UTF-8-encoded
/// string of symbols, starting in a state specified by its name.
/**
 * This converts @p word to UTF-32 and calls
 * @ref deltaHat_(std::string const&, std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a @ref std::u32string
 * on your hands, use that method.
 *
 * @param  q    the starting state&apos;s label (&isin; @ref getStates())
 * @param  word the string of symbols to process (all of which &isin;
 *              @ref getAlphabet())
 * @return      set of reached states&apos; labels
 * @throws state_not_found if @p q &notin; @ref getStates()
 * @throws symbol_not_found if one of the @p word symbols &notin;
 *              @ref getAlphabet()
 */
nfa::state_set nfa::deltaHat(string const& q, string const& word) const {
  return deltaHat_(q, converter.from_bytes(word));
}

/// Computes this NFA&apos;s transition function recursively for a string of
/// UTF-32-encoded symbols, starting in multiple specified states.
/**
 * @param  qs      valarray with `true` at starting states&apos; indices
 * @param  u32word the string of symbols to process (all of which &isin;
 *                 @ref getAlphabet_())
 * @return         valarray with `true` at reached states&apos; indices
 * @throws symbol_not_found if one of the @p u32word symbols &notin;
 *                 @ref getAlphabet_()
 */
valarray<bool> nfa::deltaHat_(valarray<bool> const& qs,
                              u32string const& u32word) const {
  valarray<bool> ps = epsilonClosure(qs);
  for (char32_t symbol : u32word) {
    valarray<bool> reached(pim->labels.size());
    size_t qi(0);
    for (bool qb : ps) {
      if (qb) {
        reached |= epsilonClosure(delta_(qi, symbol));
      }
      qi++;
    }
    ps = move(reached);
  }
  return ps;
}

/// Computes this NFA&apos;s transition function recursively for a string of
/// UTF-8-encoded symbols, starting in multiple specified states.
/**
 * This converts @p word to UTF-32 and calls
 * @ref deltaHat_(std::valarray<bool> const&, std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a @ref std::u32string
 * on your hands, use that method.
 *
 * @param  qs   valarray with `true` at starting states&apos; indices
 * @param  word the string of symbols to process (all of which &isin;
 *              @ref getAlphabet())
 * @return      valarray with `true` at reached states&apos; indices
 * @throws symbol_not_found if one of the @p word symbols &notin;
 *              @ref getAlphabet()
 */
valarray<bool> nfa::deltaHat(valarray<bool> const& qs,
                             string const& word) const {
  return deltaHat_(qs, converter.from_bytes(word));
}

/// Computes this NFA&apos;s transition function recursively for a string of
/// UTF-32-encoded symbols, starting in multiple states specified by their
/// names.
/**
 * This [encodes](@ref encodeSet) @p qs in a @ref std::valarray<bool> and calls
 * @ref deltaHat_(std::valarray<bool> const&, std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a
 * @ref std::valarray<bool> on your hands, use that method.
 *
 * @param  qs      set of starting states&apos; labels
 * @param  u32word the string of symbols to process (all of which &isin;
 *                 @ref getAlphabet_())
 * @return         set of reached states&apos; labels
 * @throws symbol_not_found if one of the @p u32word symbols &notin;
 *                 @ref getAlphabet_()
 */
nfa::state_set nfa::deltaHat_(nfa::state_set const& qs,
                              u32string const& u32word) const {
  return decodeSet(deltaHat_(encodeSet(qs), u32word));
}

/// Computes this NFA&apos;s transition function recursively for a string of
/// UTF-8-encoded symbols, starting in multiple states specified by their names.
/**
 * This converts @p word to UTF-32 and calls
 * @ref deltaHat_(std::valarray<bool> const&, std::u32string const&) const.
 * If you don&apos;t want that overhead and already have a
 * @ref std::u32string on your hands, use that method.
 *
 * @param  qs   set of starting states&apos; labels
 * @param  word the string of symbols to process (all of which &isin;
 *              @ref getAlphabet_())
 * @return      set of reached states&apos; labels
 * @throws symbol_not_found if one of the @p u32word symbols &notin;
 *              @ref getAlphabet_()
 */
nfa::state_set nfa::deltaHat(nfa::state_set const& qs,
                             string const& word) const {
  return deltaHat_(qs, converter.from_bytes(word));
}

/// Computes a state&apos;s &epsilon;-closure.
/**
 * @param  qi the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @return    valarray with `true` at indices of states reachable without actual
 *            inputs
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 */
valarray<bool> const& nfa::epsilonClosure(size_t qi) const {
  if (qi >= pim->labels.size()) {
    throw state_not_found(qi);
  }
  if (pim->epsClosures[qi].size() == 0) {
    pim->epsClosures[qi].resize(pim->labels.size());
    pim->epsClosures[qi][qi] = true;
    int growth(1);
    while (growth > 0) {
      growth = 0;
      valarray<bool> old(pim->epsClosures[qi]);
      size_t qqi(0);
      for (bool qqb : old) {
        if (qqb) {
          size_t pi(0);
          for (bool pb : pim->transitions[qqi][0]) {
            if (pb) {
              if (!pim->epsClosures[qi][pi]) {
                growth++;
                pim->epsClosures[qi][pi] = true;
              }
            }
            pi++;
          }  // going through the true bools in transitions[qqi][0]
        }
        qqi++;
      }  // going through the true bools in old
    }
  }
  return pim->epsClosures[qi];
}

/// Computes a state&apos;s &epsilon;-closure.
/**
 * This looks up the @ref index_of @p q within @ref getStates() and calls
 * @ref epsilonClosure(size_t) const.
 * If you don&apos;t want that overhead and already have a state index on your
 * hands, use that method.
 *
 * @param  q the state&apos;s label (&isin; @ref getStates)
 * @return   set of labels of states reachable without actual inputs
 * @throws state_not_found if @p q &ge; @ref getStates()`.size()`
 */
nfa::state_set nfa::epsilonClosure(string const& q) const {
  try {
    return decodeSet(epsilonClosure(index_of(getStates(), q)));
  } catch (state_not_found e) {
    throw state_not_found(q);
  }
}

/// Computes the union of multiple states&apos; &epsilon;-closures.
/**
 * @param  qs valarray with `true` at indices of states in question
 * @return    valarray with `true` at indices of states reachable without actual
 *            inputs
 */
valarray<bool> nfa::epsilonClosure(valarray<bool> const& qs) const {
  valarray<bool> closure(pim->labels.size());
  size_t range = min(qs.size(), getStates().size());
  for (size_t q = 0; q < range; q++) {
    if (qs[q]) {
      closure |= epsilonClosure(q);
    }
  }
  return closure;
}

/// Computes the union of multiple states&apos; &epsilon;-closures.
/**
 * This [encodes](@ref encodeSet) @p qs in a @ref std::valarray<bool> and calls
 * @ref epsilonClosure(std::valarray<bool> const&) const.
 * If you don&apos;t want that overhead and already have a
 * @ref std::valarray<bool> on your hands, use that method.
 *
 * @param  qs set of labels of states in question
 * @return    set of labels of states reachable without actual inputs
 */
nfa::state_set nfa::epsilonClosure(nfa::state_set const& qs) const {
  return decodeSet(epsilonClosure(encodeSet(qs)));
}

/// Puts a name to a set of indices.
/**
 * @param  qs valarray with `true` at indices of states in question
 * @return    a comma-separated list of the states&apos; names, surrounded by
 *            curly brackets
 */
string nfa::to_string(valarray<bool> const& qs) const {
  string label;
  size_t range = min(qs.size(), getStates().size());
  for (size_t q = 0; q < range; q++) {
    if (qs[q]) {
      label = label.append(1, label.length() == 0 ? '{' : ',');
      // label = label.append(1, '{' - !!label.length() * ('{' - ','));
      label = label.append(getStates()[q]);
    }
  }
  if (label.length() == 0) {
    return string("{}");
  } else {
    return label.append(1, '}');
  }
}

/// Puts a name to a set of states.
/**
 * @param  qs set of labels of states in question
 * @return    a comma-separated list of the states&apos; names, surrounded by
 *            curly brackets
 */
string nfa::to_string(nfa::state_set const& qs) const {
  return to_string(encodeSet(qs));
}

/// Translates a set of states from bool to set representation.
/**
 * @param  qs valarray with `true` at indices of states in question
 * @return    set of labels of the states in question
 */
nfa::state_set nfa::decodeSet(valarray<bool> const& qs) const {
  nfa::state_set states;
  states.reserve(getStates().size());
  size_t range = min(qs.size(), getStates().size());
  for (size_t q = 0; q < range; q++) {
    if (qs[q]) {
      states.emplace(getStates()[q]);
    }
  }
  return states;
}

/// Translates a set of states from set to bool representation.
/**
 * @param  qs set of labels of the states in question
 * @return    valarray with `true` at indices of states in question
 */
valarray<bool> nfa::encodeSet(nfa::state_set const& qs) const {
  valarray<bool> states(getStates().size());
  for (size_t qi = 0; qi < getStates().size(); qi++) {
    states[qi] = qs.count(getStates()[qi]);
  }
  return states;
}

/// Names this NFA&apos;s initial state.
/**
 * @return   the initial state&apos;s name
 */
string const& nfa::getInitialState() const { return getStates()[0]; }

/// Fetches this NFA&apos;s set of states in order of internal representation.
/**
 * @return a vector containing the names of states that can be used as input for
 *         @ref delta and @ref deltaHat
 */
vector<string> const& nfa::getStates() const { return pim->labels; }

/// Fetches this NFA&apos;s set of states as a set of (references to) strings.
/**
 * @return a set containing references to the original state names
 */
nfa::state_set nfa::getStatesSet() const {
  return nfa::state_set(getStates().begin(), getStates().end());
}

/// Fetches this NFA&apos;s set of states encoded as an array of bools.
/**
 * @return an array of length @ref getStates()`.size()`, filled with `true`
 *         values
 */
valarray<bool> nfa::getStatesBools() const {
  return valarray<bool>(true, getStates().size());
}

/// Fetches this NFA&apos;s set of processable symbols.
/**
 * @return a vector containing all the valid symbol inputs for @ref delta and
 *         @ref deltaHat
 */
vector<char32_t> const& nfa::getAlphabet_() const { return pim->u32alphabet; }

/// Fetches this NFA&apos;s set of processable symbols as UTF-8-encoded strings.
/**
 * @return a vector containing all the valid symbol inputs for @ref delta and
 *         @ref deltaHat
 */
vector<string> const& nfa::getAlphabet() const { return pim->alphabet; }

/// Tests whether a state is an accept state within this NFA.
/**
 * @param  qi the state&apos;s index (&lt; @ref getStates()`.size()`)
 * @return   `true` if the state is in the set of accept states, `false` else
 * @throws state_not_found if @p qi &ge; @ref getStates()`.size()`
 */
bool nfa::isAccepting(size_t qi) const {
  if (qi >= pim->labels.size()) {
    throw state_not_found(qi);
  }
  return pim->accepting[qi];
}

/// Tests whether a state is an accept state within this NFA.
/**
 * @param  q the state&apos;s label (&isin; @ref getStates)
 * @return   `true` if the state is in the set of accept states, `false` else
 * @throws state_not_found if @p q &notin; @ref getStates
 */
bool nfa::isAccepting(string const& q) const {
  try {
    return isAccepting(index_of(getStates(), q));
  } catch (state_not_found e) {
    throw state_not_found(q);
  }
}

/// Tests whether a set of states contains an accept state within this NFA.
/**
 * @param  qs valarray with `true` at indices of states in question
 * @return   `true` if the set of states contains an accept states, `false` else
 */
bool nfa::isAccepting(valarray<bool> const& qs) const {
  size_t range = min(qs.size(), getStates().size());
  for (size_t q = 0; q < range; q++) {
    if (qs[q] && pim->accepting[q]) {
      return true;
    }
  }
  return false;
}

/// Tests whether a set of states contains an accept state within this NFA.
/**
 * This [encodes](@ref encodeSet) @p qs in a @ref std::valarray<bool> and calls
 * @ref isAccepting(std::valarray<bool> const&) const.
 * If you don&apos;t want that overhead and already have a
 * @ref std::valarray<bool> on your hands, use that method.
 *
 * @param  qs set of labels of states in question
 * @return   `true` if the set of states contains an accept states, `false` else
 */
bool nfa::isAccepting(nfa::state_set const& qs) const {
  for (size_t qi = 0; qi < getStates().size(); qi++) {
    if (pim->accepting[qi] && qs.count(getStates()[qi])) {
      return true;
    }
  }
  return false;
}

/// Searches the shortest UTF-32-encoded word accepted by a given NFA.
/**
 * Actually finds a word that requires the minimal number of transitions to
 * reach an accept state.
 * This is not __necessarily__ the word with the shortest length.
 *
 * @param n the NFA
 * @return  the shortest word leading to one of the NFA&apos;s accept states
 * @throws std::logic_error if the NFA doesn&apos;t accept any words
 */
u32string findShortestWord_(nfa const& n) {
  auto const& pim = n.pim;
  if (pim->accepting[0]) {
    return U"";
  }
  unordered_map<size_t, u32string> shortestWords(pim->labels.size());
  size_t oldSize = 0;
  shortestWords.emplace(0, U"");
  while (shortestWords.size() > oldSize) {
    oldSize = shortestWords.size();
    for (auto const& stateWord : shortestWords) {
      for (auto symbol : n.getAlphabet_()) {
        valarray<bool> reached =
            n.deltaHat_(stateWord.first, u32string(!!symbol, symbol));
        u32string shWord = stateWord.second + u32string(!!symbol, symbol);
        for (size_t q = 0; q < reached.size(); q++) {
          if (reached[q]) {
            if (pim->accepting[q]) {
              return shWord;
            }
            if (!shortestWords.count(q)) {
              shortestWords.emplace(q, shWord);
            }
          }
        }
      }
    }
  }
  throw std::logic_error("This NFA doesn't accept any words!");
}

/// Searches the shortest UTF-8-encoded word accepted by a given NFA.
/**
 * Actually finds a word that requires the minimal number of transitions to
 * reach an accept state.
 * Due to &epsilon;-transitions, this is not **necessarily** the word with the
 * shortest length.
 *
 * This calls @ref findShortestWord_(nfa const&) and converts the result to
 * UTF-8.
 * If you don&apos;t want that overhead and can handle UTF-32-encoded strings,
 * use that function.
 *
 * @param n the NFA
 * @return  the shortest word leading to one of the NFA&apos;s accept states
 * @throws std::logic_error if the NFA doesn&apos;t accept any words
 */
string findShortestWord(nfa const& n) {
  return converter.to_bytes(findShortestWord_(n));
}

/// Creates a builder for an NFA accepting the union of the languages of two
/// NFAs.
/**
 * Concatenates the names of states defined so far with the other NFA's state
 * names and resolves conflicts by appending `_`.
 *
 * @param n1 the first NFA
 * @param n2 the other NFA
 * @return   builder for an NFA accepting all the words accepted by any of the
 *           input NFAs
 */
fabuilder nfa::unite(nfa const& n1, nfa const& n2) {
  return fabuilder(n1).unite(n2);
}

/// Creates a builder for an NFA accepting the intersection of the languages of
/// two NFAs.
/**
 * The input NFAs' state names will be concatenated and collisions resolved by
 * appending `_` in the created NFA.
 *
 * @param n1 the first NFA
 * @param n2 the other NFA
 * @return   builder for an NFA accepting all the words accepted by both of the
 *           input NFAs
 */
fabuilder nfa::intersect(nfa const& n1, nfa const& n2) {
  return fabuilder(n1).intersect(n2);
}

/// Creates a builder for an NFA accepting the set difference of the languages
/// of two NFAs.
/**
 * The input NFAs' state names will probably be mangled in the created NFA.
 *
 * @param n1 the first NFA
 * @param n2 the other NFA
 * @return   builder for an NFA accepting all the words accepted by the first
 *           but not the other input NFA
 */
fabuilder nfa::subtract(nfa const& n1, nfa const& n2) {
  fabuilder b2(n2);
  for (auto symbol : n1.getAlphabet_()) {
    b2.addSymbol_(symbol);
  }
  b2.complement(true).minimize();
  return fabuilder(n1).intersect(b2.buildNfa());
}

/// Creates a builder for an NFA accepting the complement of the language of an
/// NFA.
/**
 * The input NFAs' state names will probably be mangled in the created NFA.
 *
 * @param n the NFA
 * @return  builder for an NFA accepting all words not accepted by the input NFA
 *          (provided they can be built from symbols of that NFA&apos;s
 *          [alphabet](@ref getAlphabet))
 */
fabuilder nfa::complement(nfa const& n) {
  return fabuilder(n).complement(true).minimize();
}

/// Copy-constructs an NFA by copying another one&apos;s
/// [private implementation object](@ref impl).
nfa::nfa(nfa const& n) : pim(new impl(*(n.pim))) {}

/// Move-constructs an NFA by stealing another one&apos;s
/// [private implementation object](@ref impl).
/** The other NFA will be accepting the empty language &empty; afterwards. */
nfa::nfa(nfa&& n) : pim(new impl) { pim.swap(n.pim); }

/// Copy-assigns this NFA by copying another one&apos;s
/// [private implementation object](@ref impl).
nfa& nfa::operator=(nfa const& n) {
  if (this != &n) {
    pim.reset(new impl(*(n.pim)));
  }
  return *this;
}

/// Move-assigns this NFA by stealing another one&apos;s
/// [private implementation object](@ref impl).
/** The other NFA will be accepting the empty language &empty; afterwards. */
nfa& nfa::operator=(nfa&& n) {
  if (this != &n) {
    pim.reset(new impl);
    pim.swap(n.pim);
  }
  return *this;
}

nfa::~nfa() = default;

}  // namespace reg
