// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

/// Contains test cases for the @ref reg::dfa class. @file
#include "../dfa.h"
#include "../nfa.h"
#include "../fabuilder.h"
#include "gtest/gtest.h"

#include <string>
using std::string;

namespace {

TEST(DfaImplementation, Constructors) {
  using namespace reg;
  dfa d1;
  dfa d2(d1);
  dfa d3(std::move(d1));
}

TEST(DfaImplementation, Members) {
  using namespace reg;
  dfa d;
  d.operator==(d);
  d.operator!=(d);
  EXPECT_THROW(d.delta(0, 0), symbol_not_found);
  EXPECT_THROW(d.delta_(0, U'\0'), symbol_not_found);
  EXPECT_THROW(d.delta(0, ""), symbol_not_found);
  EXPECT_THROW(d.delta_("", U'\0'), symbol_not_found);
  EXPECT_THROW(d.delta("", ""), symbol_not_found);
  d.deltaHat_(0, U"");
  d.deltaHat(0, "");
  d.deltaHat_("", U"");
  d.deltaHat("", "");
  EXPECT_THROW(findShortestWord(d), std::logic_error);
  EXPECT_THROW(findShortestWord(d), std::logic_error);
  d.getInitialState();
  d.getStates();
  d.getAlphabet();
  d.getAlphabet();
  d.isAccepting(0);
  d.isAccepting("");
}

TEST(DfaImplementation, Static) {
  using namespace reg;
  dfa d;
  EXPECT_THROW(findShortestWord(d), std::logic_error);
  EXPECT_THROW(findShortestWord(d), std::logic_error);
  dfa::unite(d, d);
  dfa::intersect(d, d);
  dfa::subtract(d, d);
  dfa::complement(d);
}

TEST(DfaTest, SimplestBuilderTest) {
  using namespace reg;
  fabuilder b;
  b.makeInitial("qE").setAccepting("qE", true);
  dfa d = b.buildDfa();
  EXPECT_EQ(d.deltaHat_(0, U""), 0);
  EXPECT_EQ(d.deltaHat(0, ""), 0);
  EXPECT_TRUE(d.isAccepting(0));
  EXPECT_EQ(d.getStates().at(0), "qE");
  b = fabuilder();
  b.makeInitial("q0");
  d = b.buildDfa();
  EXPECT_EQ(d.deltaHat_(0, U""), 0);
  EXPECT_EQ(d.deltaHat(0, ""), 0);
  EXPECT_FALSE(d.isAccepting(0));
  EXPECT_EQ(d.getStates().at(0), "q0");
}

TEST(DfaTest, SimpleBuilderTest) {
  using namespace reg;
  fabuilder b(fabuilder().makeInitial("q0").buildDfa());
  fabuilder bu8(b);
  b.addTransition_("q0", "qE", U'1');
  bu8.addTransition("q0", "qE", "1");
  b.setAccepting("qE", true);
  bu8.setAccepting("qE", true);
  dfa d = b.buildDfa();
  dfa du8 = bu8.buildDfa();
  EXPECT_EQ(d.deltaHat_(0, U""), 0);
  EXPECT_EQ(d.getStates().at(d.deltaHat_(0, U"1")), "qE");
  EXPECT_EQ(d.deltaHat(0, ""), 0);
  EXPECT_EQ(d.getStates().at(d.deltaHat(0, "1")), "qE");
  EXPECT_FALSE(d.isAccepting(0));
  EXPECT_EQ(d.getStates().at(0), "q0");
  EXPECT_EQ(du8.deltaHat_(0, U""), 0);
  EXPECT_EQ(du8.getStates().at(du8.deltaHat_(0, U"1")), "qE");
  EXPECT_EQ(du8.deltaHat(0, ""), 0);
  EXPECT_EQ(du8.getStates().at(du8.deltaHat(0, "1")), "qE");
  EXPECT_FALSE(du8.isAccepting(0));
  EXPECT_EQ(du8.getStates().at(0), "q0");
}

TEST(DfaTest, SlightlyMoreComplicatedBuilderTest) {
  using namespace reg;
  nfa n = fabuilder()
      .addTransition_("q0", "q0", U'0').addTransition_("q0", "q0", U'1')
      .addTransition_("q0", "q1", U'0').addTransition_("q0", "q1")
      .addTransition_("q0", "q2", U'1').addTransition_("q1", "q3", U'1')
      .addTransition_("q2", "q1").addTransition_("q2", "q1", U'1')
      .addTransition_("q2", "q3", U'0').setAccepting("q3", true).buildNfa();
  dfa d = fabuilder(n).buildDfa(true);
  for (string const& p : n.decodeSet(n.deltaHat_(0, U""))) {
    EXPECT_NE(d.getStates().at(0).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat_(0, U"1"))) {
    EXPECT_NE(d.getStates().at(d.deltaHat_(0, U"1")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat_(0, U""))) {
    EXPECT_NE(d.getStates().at(d.deltaHat_(0, U"0")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat_(0, U"1"))) {
    EXPECT_NE(d.getStates().at(d.deltaHat_(0, U"11")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat_(0, U"10"))) {
    EXPECT_NE(d.getStates().at(d.deltaHat_(0, U"10")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat_(0, U""))) {
    EXPECT_NE(d.getStates().at(d.deltaHat_(0, U"100")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat(0, ""))) {
    EXPECT_NE(d.getStates().at(0).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat(0, "1"))) {
    EXPECT_NE(d.getStates().at(d.deltaHat(0, "1")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat(0, ""))) {
    EXPECT_NE(d.getStates().at(d.deltaHat(0, "0")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat(0, "1"))) {
    EXPECT_NE(d.getStates().at(d.deltaHat(0, "11")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat(0, "10"))) {
    EXPECT_NE(d.getStates().at(d.deltaHat(0, "10")).find(p), std::string::npos);
  }
  for (string const& p : n.decodeSet(n.deltaHat(0, ""))) {
    EXPECT_NE(d.getStates().at(d.deltaHat(0, "100")).find(p), std::string::npos);
  }
}

TEST(DfaTest, MinimizeTest) {
  using namespace reg;
  fabuilder b;
  for (size_t i = 0; i < 7; i++) {
    b.addTransition_('0' + std::to_string(i), '1' + std::to_string((i+2)%7), U'1');
    b.addTransition_('0' + std::to_string(i), '1' + std::to_string(i), U'0');
    b.addTransition_('1' + std::to_string(i), '2' + std::to_string((i+4)%7), U'1');
    b.addTransition_('1' + std::to_string(i), '2' + std::to_string(i), U'0');
    b.addTransition_('2' + std::to_string(i), '0' + std::to_string((i+1)%7), U'1');
    b.addTransition_('2' + std::to_string(i), '0' + std::to_string(i), U'0');
  }
  b.setAccepting("00", true);
  b.setAccepting("10", true);
  b.setAccepting("20", true);
  fabuilder c(b);
  dfa a(c);
  ASSERT_EQ(a.getStates().size(), 21);
  c = b.purge();
  EXPECT_EQ(c.buildDfa().getStates().size(), 21);
  c = b.merge();
  dfa d(c);
  EXPECT_EQ(d.getStates().size(), 7);
  EXPECT_EQ(d, a);
}

TEST(DfaTest, NormalizationTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("p0", "p1", "0");
  b.addTransition("p1", "p2", "0");
  b.setAccepting("p2", true);
  dfa d(b);
  ASSERT_EQ(d.getStates()[0], "p0");
  ASSERT_NE(index_of(d.getStates(), string("p1")), d.getStates().size());
  ASSERT_NE(index_of(d.getStates(), string("p2")), d.getStates().size());
  ASSERT_NE(index_of(d.getStates(), string("q0")), d.getStates().size());
  ASSERT_TRUE(d.isAccepting(index_of(d.getStates(), string("p2"))));
  for (size_t i = 0; i < 100; i++) {
    size_t reached = d.deltaHat(0, string(i, '0'));
    if (i == 2) {
      for (size_t p = 0; p < d.getStates().size(); p++) {
        ASSERT_EQ(p == reached, d.isAccepting(p));
      }
    } else {
      ASSERT_FALSE(d.isAccepting(reached));
    }
  }
  b.normalizeStateNames("q");
  d = b.buildDfa();
  EXPECT_EQ(d.getStates()[0], "q0");
  EXPECT_NE(index_of(d.getStates(), string("q1")), d.getStates().size());
  EXPECT_NE(index_of(d.getStates(), string("q2")), d.getStates().size());
  EXPECT_NE(index_of(d.getStates(), string("q3")), d.getStates().size());
  for (size_t i = 0; i < 100; i++) {
    size_t reached = d.deltaHat(0, string(i, '0'));
    if (i == 2) {
      for (size_t p = 0; p < d.getStates().size(); p++) {
        EXPECT_EQ(d.isAccepting(p), p == reached);
      }
    } else {
      EXPECT_FALSE(d.isAccepting(reached));
    }
  }
}

TEST(DfaTest, ComplementTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("p0", "p1", "0");
  b.addTransition("p1", "p2", "0");
  b.setAccepting("p2", true);
  dfa d(b);
  ASSERT_EQ(d.getStates()[0], "p0");
  ASSERT_NE(index_of(d.getStates(), string("p1")), d.getStates().size());
  ASSERT_NE(index_of(d.getStates(), string("p2")), d.getStates().size());
  ASSERT_NE(index_of(d.getStates(), string("q0")), d.getStates().size());
  ASSERT_TRUE(d.isAccepting(index_of(d.getStates(), string("p2"))));
  for (size_t i = 0; i < 100; i++) {
    size_t reached = d.deltaHat(0, string(i, '0'));
    if (i == 2) {
      for (size_t p = 0; p < d.getStates().size(); p++) {
        ASSERT_EQ(p == reached, d.isAccepting(p));
      }
    } else {
      ASSERT_FALSE(d.isAccepting(reached));
    }
  }
  b.complement();
  d = b.buildDfa();
  for (size_t i = 0; i < 100; i++) {
    size_t reached = d.deltaHat(0, string(i, '0'));
    if (i == 2) {
      for (size_t p = 0; p < d.getStates().size(); p++) {
        EXPECT_EQ(d.isAccepting(p), p != reached);
      }
    } else {
      EXPECT_TRUE(d.isAccepting(reached));
    }
  }
}

TEST(DfaTest, UnionTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("p0", "p1", "0");
  b.addTransition("p1", "p2", "0");
  b.setAccepting("p2", true);
  dfa d(b);
  ASSERT_EQ(d.getStates()[0], "p0");
  ASSERT_NE(index_of(d.getStates(), string("p1")), d.getStates().size());
  ASSERT_NE(index_of(d.getStates(), string("p2")), d.getStates().size());
  ASSERT_TRUE(d.isAccepting(index_of(d.getStates(), string("p2"))));
  size_t reached = d.deltaHat(0, "00");
  for (size_t p = 0; p < d.getStates().size(); p++) {
    ASSERT_EQ(reached == p, d.isAccepting(p));
  }
  b = fabuilder();
  b.addTransition("p0", "p1", "1");
  b.addTransition("p1", "p2", "1");
  b.setAccepting("p2", true);
  b.unite(d);
  d = b.buildDfa();
  EXPECT_EQ(d.getStates().size(), 20);
  EXPECT_EQ(d.getStates()[0], "p0p0");
  reached = d.deltaHat(0, "00");
  EXPECT_TRUE(d.isAccepting(reached));
  reached = d.deltaHat(0, "11");
  EXPECT_TRUE(d.isAccepting(reached));
}

TEST(DfaTest, IntersectionTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("0", "000", "1");
  b.addTransition("0", "00", "0");
  b.addTransition("00", "000", "1");
  b.addTransition("00", "0000", "0");
  b.addTransition("000", "000", "1");
  b.addTransition("000", "00", "0");
  b.addTransition("0000", "0000", "1");
  b.addTransition("0000", "0000", "0");
  b.setAccepting("0", true);
  b.setAccepting("0000", true);
  fabuilder c;
  c.addTransition("0", "000", "0");
  c.addTransition("0", "00", "1");
  c.addTransition("00", "000", "0");
  c.addTransition("00", "0000", "1");
  c.addTransition("000", "000", "0");
  c.addTransition("000", "00", "1");
  c.addTransition("0000", "0000", "0");
  c.addTransition("0000", "0000", "1");
  c.setAccepting("0", true);
  c.setAccepting("0000", true);
  dfa e(c);
  size_t reached;
  reached = e.deltaHat(0, "");
  ASSERT_TRUE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "1100");
  ASSERT_TRUE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "0011");
  ASSERT_TRUE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "11100");
  ASSERT_TRUE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "00111");
  ASSERT_TRUE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "11");
  ASSERT_TRUE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "00");
  ASSERT_FALSE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "1");
  ASSERT_FALSE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "0");
  ASSERT_FALSE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "10");
  ASSERT_FALSE(e.isAccepting(reached)) << e.getStates().at(reached);
  reached = e.deltaHat(0, "01");
  ASSERT_FALSE(e.isAccepting(reached)) << e.getStates().at(reached);
  b.intersect(c.buildDfa());
  dfa d(b);
  EXPECT_EQ(d.getStates().size(), 16);
  reached = d.deltaHat(0, "");
  EXPECT_TRUE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "1100");
  EXPECT_TRUE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "0011");
  EXPECT_TRUE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "11100");
  EXPECT_TRUE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "00111");
  EXPECT_TRUE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "11");
  EXPECT_FALSE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "00");
  EXPECT_FALSE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "1");
  EXPECT_FALSE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "0");
  EXPECT_FALSE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "10");
  EXPECT_FALSE(d.isAccepting(reached)) << d.getStates().at(reached);
  reached = d.deltaHat(0, "01");
  EXPECT_FALSE(d.isAccepting(reached)) << d.getStates().at(reached);
}

TEST(DfaTest, ShortestWordTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("0", "000", "1");
  b.addTransition("0", "00", "0");
  b.addTransition("00", "000", "1");
  b.addTransition("00", "0000", "0");
  b.addTransition("000", "000", "1");
  b.addTransition("000", "00", "0");
  b.addTransition("0000", "0000", "1");
  b.addTransition("0000", "0000", "0");
  b.setAccepting("0000", true);
  dfa d(b);
  EXPECT_EQ(findShortestWord(d), "00");
  d = b.setAccepting("0", true).buildDfa();
  EXPECT_EQ(findShortestWord(d), "");
  b = fabuilder();
  for (size_t i = 0; i < 7; i++) {
    b.addTransition_('0' + std::to_string(i), '1' + std::to_string((i+2)%7), U'1');
    b.addTransition_('0' + std::to_string(i), '1' + std::to_string(i), U'0');
    b.addTransition_('1' + std::to_string(i), '2' + std::to_string((i+4)%7), U'1');
    b.addTransition_('1' + std::to_string(i), '2' + std::to_string(i), U'0');
    b.addTransition_('2' + std::to_string(i), '0' + std::to_string((i+1)%7), U'1');
    b.addTransition_('2' + std::to_string(i), '0' + std::to_string(i), U'0');
  }
  b.setAccepting("00", true);
  b.setAccepting("10", true);
  b.setAccepting("20", true);
  d = b.complement().unite(fabuilder().setAccepting("1", true).addTransition_("1", "1", U'0').addSymbol_(U'1').buildDfa()).complement().buildDfa();
  EXPECT_EQ(findShortestWord(d), "111");
}

} // namespace
