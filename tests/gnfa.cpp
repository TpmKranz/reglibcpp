// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

/// Contains test cases for the @ref reg::gnfa class. @file
#include "../nfa.h"
#include "../gnfa.h"
#include "../fabuilder.h"
#include "gtest/gtest.h"

#include <string>
using std::string;

namespace {

TEST(GnfaTest, SimplestBuilderTest) {
  using namespace reg;
  gnfa g(expression::spawnConcatenation(expression::spawnSymbol("a"), expression::spawnSymbol("b")));
  EXPECT_EQ(g.getTransition(g.getInitialState(), g.getAcceptingState())->to_string(), "ab");
  EXPECT_EQ(g.getTransition(g.getAcceptingState(), g.getInitialState())->to_string(), u8"∅");
  EXPECT_EQ(g.ripAllStates()->to_string(), "ab");
  EXPECT_TRUE(g.getActiveStates().empty());
  gnfa h(fabuilder().addTransition("0", "1", "a").addTransition("0", "1", "b").setAccepting("0", true).setAccepting("1", true).buildNfa());
  EXPECT_EQ(h.getTransition(h.getInitialState(), "0")->to_string(), u8"ε");
  EXPECT_EQ(h.getTransition(h.getInitialState(), "1")->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition(h.getInitialState(), h.getAcceptingState())->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition("0", "0")->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition("0", "1")->to_string(), "a+b");
  EXPECT_EQ(h.getTransition("0", h.getAcceptingState())->to_string(), u8"ε");
  EXPECT_EQ(h.getTransition("0", h.getInitialState())->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition("1", "1")->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition("1", h.getAcceptingState())->to_string(), u8"ε");
  EXPECT_EQ(h.getTransition("1", h.getInitialState())->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition("1", "0")->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition(h.getAcceptingState(), h.getAcceptingState())->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition(h.getAcceptingState(), h.getInitialState())->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition(h.getAcceptingState(), "0")->to_string(), u8"∅");
  EXPECT_EQ(h.getTransition(h.getAcceptingState(), "1")->to_string(), u8"∅");
}

TEST(GnfaTest, BasicFunctionalityTest) {
  using namespace reg;
  expression::exptr re = expression::spawnConcatenation(
      expression::spawnSymbol("a"),
      expression::spawnAlternation(
          expression::spawnKleene(
              expression::spawnSymbol("b")
          ),
          expression::spawnSymbol("c")
      )
  );
  gnfa g(re);
  ASSERT_EQ(g.ripAllStates()->to_string(), "a(b*+c)");
  EXPECT_EQ(
      fabuilder()
          .addTransition("a", "b*+c", "a")
          .addTransition("b*+c", "b*","")
          .addTransition("b*", "b*", "b")
          .addTransition("b*+c", "c", "c")
          .setAccepting("b*", true)
          .setAccepting("c", true)
          .buildNfa(),
      g.splitAllTransitions()
  );
  EXPECT_EQ(g.splitTransition(g.getInitialState(), g.getAcceptingState()).size(), 0);
  EXPECT_EQ(*g.ripAllStates(), *re);
}

} // namespace
