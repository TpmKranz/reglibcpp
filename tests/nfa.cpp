// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

/// Contains test cases for the @ref reg::nfa class. @file
#include "../nfa.h"
#include "../dfa.h"
#include "../fabuilder.h"
#include "gtest/gtest.h"

#include <string>
using std::string;

namespace {

TEST(NfaImplementation, Constructors) {
  using namespace reg;
  nfa n1;
  nfa n2(n1);
  nfa n3(std::move(n1));
}

TEST(NfaImplementation, Members) {
  using namespace reg;
  nfa n;
  dfa d = n;
  n.operator==(d);
  n.operator!=(d);
  n.operator==(n);
  n.operator!=(n);
  n.delta(0, (size_t) 0);
  n.delta_(0, U'\0');
  n.delta(0, "");
  n.delta_("", U'\0');
  n.delta("", "");
  n.deltaHat_(0, U"");
  n.deltaHat(0, "");
  n.deltaHat_("", U"");
  n.deltaHat("", "");
  n.deltaHat_(std::valarray<bool>(), U"");
  n.deltaHat(std::valarray<bool>(), "");
  n.deltaHat_(nfa::state_set(), U"");
  n.deltaHat(nfa::state_set(), "");
  n.epsilonClosure(0);
  n.epsilonClosure("");
  n.epsilonClosure(std::valarray<bool>());
  n.epsilonClosure(nfa::state_set());
  EXPECT_THROW(findShortestWord(n), std::logic_error);
  EXPECT_THROW(findShortestWord(n), std::logic_error);
  n.to_string(std::valarray<bool>());
  n.to_string(nfa::state_set());
  n.decodeSet(std::valarray<bool>());
  n.encodeSet(nfa::state_set());
  n.getInitialState();
  n.getStates();
  n.getStatesSet();
  n.getStatesBools();
  n.getAlphabet();
  n.getAlphabet();
  n.isAccepting(0);
  n.isAccepting("");
  n.isAccepting(std::valarray<bool>());
  n.isAccepting(nfa::state_set());
}

TEST(NfaImplementation, Static) {
  using namespace reg;
  nfa n;
  EXPECT_THROW(findShortestWord(n), std::logic_error);
  EXPECT_THROW(findShortestWord(n), std::logic_error);
  nfa::unite(n, n);
  nfa::intersect(n, n);
  nfa::subtract(n, n);
  nfa::complement(n);
}

TEST(NfaTest, SimplestBuilderTest) {
  using namespace reg;
  fabuilder b;
  b.makeInitial("qE").setAccepting("qE", true);
  nfa n = b.buildNfa();
  EXPECT_TRUE(n.deltaHat_(0, U"")[0]);
  EXPECT_TRUE(n.deltaHat(0, "")[0]);
  EXPECT_TRUE(n.isAccepting(0));
  EXPECT_EQ(n.getStates().at(0), "qE");
  b = fabuilder();
  b.makeInitial("q0");
  n = b.buildNfa();
  EXPECT_TRUE(n.deltaHat_(0, U"")[0]);
  EXPECT_TRUE(n.deltaHat(0, "")[0]);
  EXPECT_FALSE(n.isAccepting(0));
  EXPECT_EQ(n.getStates().at(0), "q0");
}

TEST(NfaTest, SimpleBuilderTest) {
  using namespace reg;
  fabuilder b(fabuilder().makeInitial("q0").buildNfa());
  fabuilder bu8(b);
  b.addTransition_("q0", "qE", U'1');
  bu8.addTransition("q0", "qE", "1");
  b.setAccepting("qE", true);
  bu8.setAccepting("qE", true);
  nfa n = b.buildNfa();
  nfa nu8 = bu8.buildNfa();
  EXPECT_TRUE(n.deltaHat_(0, U"")[0]);
  EXPECT_FALSE(n.deltaHat_(0, U"")[1]);
  EXPECT_FALSE(n.deltaHat_(0, U"1")[0]);
  EXPECT_TRUE(n.deltaHat_(0, U"1")[1]);
  EXPECT_TRUE(n.deltaHat(0, "")[0]);
  EXPECT_FALSE(n.deltaHat(0, "")[1]);
  EXPECT_FALSE(n.deltaHat(0, "1")[0]);
  EXPECT_TRUE(n.deltaHat(0, "1")[1]);
  EXPECT_FALSE(n.isAccepting(0));
  EXPECT_TRUE(n.isAccepting(1));
  EXPECT_EQ(n.getStates().at(0), "q0");
  EXPECT_EQ(n.getStates().at(1), "qE");
  EXPECT_TRUE(nu8.deltaHat_(0, U"")[0]);
  EXPECT_FALSE(nu8.deltaHat_(0, U"")[1]);
  EXPECT_FALSE(nu8.deltaHat_(0, U"1")[0]);
  EXPECT_TRUE(nu8.deltaHat_(0, U"1")[1]);
  EXPECT_TRUE(nu8.deltaHat(0, "")[0]);
  EXPECT_FALSE(nu8.deltaHat(0, "")[1]);
  EXPECT_FALSE(nu8.deltaHat(0, "1")[0]);
  EXPECT_TRUE(nu8.deltaHat(0, "1")[1]);
  EXPECT_FALSE(nu8.isAccepting(0));
  EXPECT_TRUE(nu8.isAccepting(1));
  EXPECT_EQ(nu8.getStates().at(0), "q0");
  EXPECT_EQ(nu8.getStates().at(1), "qE");
}

TEST(NfaTest, SlightlyMoreComplicatedBuilderTest) {
  using namespace reg;
  dfa d = fabuilder()
      .addTransition_("q0", "q0", U'0').addTransition_("q0", "q1", U'1')
      .addTransition_("q1", "q2", U'0').addTransition_("q1", "q1", U'1')
      .addTransition_("q2", "q0", U'0').addTransition_("q2", "q1", U'1')
      .setAccepting("q1", true).setAccepting("q2", true)
      .buildDfa();
  nfa n = fabuilder(d).buildNfa();
  EXPECT_EQ(n.to_string(n.deltaHat_(0, U"")), '{' + d.getStates().at(0) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat_(0, U"1")), '{' + d.getStates().at(d.deltaHat_(0, U"1")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat_(0, U"")), '{' + d.getStates().at(d.deltaHat_(0, U"0")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat_(0, U"1")), '{' + d.getStates().at(d.deltaHat_(0, U"11")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat_(0, U"10")), '{' + d.getStates().at(d.deltaHat_(0, U"10")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat_(0, U"")), '{' + d.getStates().at(d.deltaHat_(0, U"100")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat(0, "")), '{' + d.getStates().at(0) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat(0, "1")), '{' + d.getStates().at(d.deltaHat(0, "1")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat(0, "")), '{' + d.getStates().at(d.deltaHat(0, "0")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat(0, "1")), '{' + d.getStates().at(d.deltaHat(0, "11")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat(0, "10")), '{' + d.getStates().at(d.deltaHat(0, "10")) + '}');
  EXPECT_EQ(n.to_string(n.deltaHat(0, "")), '{' + d.getStates().at(d.deltaHat(0, "100")) + '}');
}

TEST(NfaTest, NormalizationTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("p0", "p1", "0");
  b.addTransition("p1", "p2", "0");
  b.setAccepting("p2", true);
  nfa n(b);
  std::vector<string> stateNames;
  for (size_t p = 0; p < n.getStates().size(); p++) {
    stateNames.push_back(n.getStates().at(p));
  }
  ASSERT_EQ(stateNames[0], "p0");
  ASSERT_NE(index_of(stateNames, string("p1")), stateNames.size());
  ASSERT_NE(index_of(stateNames, string("p2")), stateNames.size());
  ASSERT_TRUE(n.isAccepting(index_of(stateNames, string("p2"))));
  std::valarray<bool> reached = n.deltaHat(0, "00");
  for (size_t p = 0; p < reached.size(); p++) {
    ASSERT_EQ(reached[p], n.isAccepting(p));
  }
  b.normalizeStateNames("q");
  n = b.buildNfa();
  stateNames.clear();
  for (size_t q = 0; q < n.getStates().size(); q++) {
    stateNames.push_back(n.getStates().at(q));
  }
  EXPECT_EQ(stateNames[0], "q0");
  EXPECT_NE(index_of(stateNames, string("q1")), stateNames.size());
  EXPECT_NE(index_of(stateNames, string("q2")), stateNames.size());
  reached = n.deltaHat(0, "00");
  for (size_t q = 0; q < reached.size(); q++) {
    EXPECT_EQ(n.isAccepting(q), reached[q]);
  }
}

TEST(NfaTest, UnionTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("p0", "p1", "0");
  b.addTransition("p1", "p2", "0");
  b.setAccepting("p2", true);
  nfa n(b);
  std::vector<string> stateNames = n.getStates();
  ASSERT_EQ(stateNames.size(), 3);
  ASSERT_EQ(stateNames[0], "p0");
  ASSERT_NE(index_of(stateNames, string("p1")), stateNames.size());
  ASSERT_NE(index_of(stateNames, string("p2")), stateNames.size());
  ASSERT_TRUE(n.isAccepting(index_of(stateNames, string("p2"))));
  std::valarray<bool> reached = n.deltaHat(0, "00");
  for (size_t p = 0; p < reached.size(); p++) {
    ASSERT_EQ(reached[p], n.isAccepting(p));
  }
  b = fabuilder();
  b.addTransition("p0", "p1", "1");
  b.addTransition("p1", "p2", "1");
  b.setAccepting("p2", true);
  b.unite(n);
  n = b.buildNfa();
  EXPECT_EQ(n.getStates().size(), (stateNames.size() + 1) * (stateNames.size() + 1));
  EXPECT_EQ(n.getStates()[0], "p0p0");
  for (string const& q : stateNames) {
    for (string const& qq : stateNames) {
      EXPECT_NE(index_of(n.getStates(), q+qq), n.getStates().size());
    }
  }
  EXPECT_TRUE(n.isAccepting(n.deltaHat(0, "00")));
  EXPECT_TRUE(n.isAccepting(n.deltaHat(0, "11")));
  for (size_t p = 0; p < n.getStates().size(); p++) {
    EXPECT_EQ(n.getStates()[p].find("p2") != std::string::npos, n.isAccepting(p));
  }
}

TEST(NfaTest, IntersectionTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("0",    "00",    "");
  b.addTransition("0",    "00000", "");
  b.addTransition("00",   "00",    "1");
  b.addTransition("00",   "00",    "0");
  b.addTransition("00",   "000",   "0");
  b.addTransition("000",  "0000",  "0");
  b.addTransition("0000", "0000",  "0");
  b.addTransition("0000", "0000",  "1");
  b.addTransition("0000", "00000", "");
  b.setAccepting("00000", true);
  fabuilder c;
  c.addTransition("0",    "00",    "");
  c.addTransition("0",    "00000", "");
  c.addTransition("00",   "00",    "1");
  c.addTransition("00",   "00",    "0");
  c.addTransition("00",   "000",   "1");
  c.addTransition("000",  "0000",  "1");
  c.addTransition("0000", "0000",  "0");
  c.addTransition("0000", "0000",  "1");
  c.addTransition("0000", "00000", "");
  c.setAccepting("00000", true);
  nfa m(c);
  size_t acc = 0;
  for (size_t q = 0; q < m.getStates().size(); q++) {
    if (m.isAccepting(q)) {
      acc = q;
      break;
    }
  }
  ASSERT_EQ(m.getStates().at(0), "0");
  ASSERT_EQ(m.getStates().at(acc), "00000");
  std::valarray<bool> reached = m.deltaHat(0, "");
  for (size_t q = 0; q < m.getStates().size(); q++) {
    ASSERT_EQ(reached[q], q == 0 || q == acc || m.getStates().at(q) == "00");
  }
  reached = m.deltaHat(0, "1100");
  ASSERT_TRUE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "0011");
  ASSERT_TRUE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "11100");
  ASSERT_TRUE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "00111");
  ASSERT_TRUE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "11");
  ASSERT_TRUE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "00");
  ASSERT_FALSE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "1");
  ASSERT_FALSE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "0");
  ASSERT_FALSE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "10");
  ASSERT_FALSE(m.isAccepting(reached)) << m.to_string(reached);
  reached = m.deltaHat(0, "01");
  ASSERT_FALSE(m.isAccepting(reached)) << m.to_string(reached);
  b.intersect(m);
  nfa n(b);
  EXPECT_EQ(n.getStates().size(), 25);
  reached = n.deltaHat(0, "");
  EXPECT_TRUE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "1100");
  EXPECT_TRUE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "0011");
  EXPECT_TRUE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "11100");
  EXPECT_TRUE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "00111");
  EXPECT_TRUE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "11");
  EXPECT_FALSE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "00");
  EXPECT_FALSE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "1");
  EXPECT_FALSE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "0");
  EXPECT_FALSE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "10");
  EXPECT_FALSE(n.isAccepting(reached)) << n.to_string(reached);
  reached = n.deltaHat(0, "01");
  EXPECT_FALSE(n.isAccepting(reached)) << n.to_string(reached);
}

TEST(NfaTest, ShortestWordTest) {
  using namespace reg;
  fabuilder b;
  b.addTransition("0",    "00");
  b.addTransition("00",   "00",    "1");
  b.addTransition("00",   "00",    "0");
  b.addTransition("00",   "000",   "0");
  b.addTransition("000",  "0000",  "0");
  b.addTransition("0000", "0000",  "0");
  b.addTransition("0000", "0000",  "1");
  b.addTransition("0000", "00000");
  b.setAccepting("00000", true);
  nfa n(b);
  EXPECT_EQ(findShortestWord(n), "00");
  n = b.addTransition("0",    "00000").buildNfa();
  EXPECT_EQ(findShortestWord(n), "");
}

} // namespace
