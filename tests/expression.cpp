// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

/// Contains test cases for the @ref reg::expression class. @file
#include "../expression.h"
#include "../nfa.h"
#include "gtest/gtest.h"

#include <string>
using std::string;

namespace {

TEST(ExpressionTest, BasicFunctionalityTest) {
  using namespace reg;
  auto empty = expression::spawnEmptySet();
  EXPECT_EQ(empty->getOperation(), expression::operation::empty);
  EXPECT_EQ(expression::spawnEmptySet(), empty);
  auto epsilon = expression::spawnSymbol_(U'\0');
  EXPECT_EQ(epsilon, expression::spawnEmptyString());
  auto a = expression::spawnSymbol_(U'a');
  auto b = expression::spawnSymbol_(U'b');
  EXPECT_EQ(epsilon->getOperation(), expression::operation::symbol);
  EXPECT_EQ(epsilon->extractSymbol_(), U'\0');
  EXPECT_EQ(epsilon->extractSymbol(), "");
  EXPECT_EQ(epsilon->to_u32string(), U"ε");
  EXPECT_EQ(epsilon->to_string(), u8"ε");
  EXPECT_EQ(a->getOperation(), expression::operation::symbol);
  EXPECT_EQ(a->extractSymbol_(), U'a');
  EXPECT_EQ(a->extractSymbol(), "a");
  EXPECT_EQ(a->to_u32string(), U"a");
  EXPECT_EQ(a->to_string(), "a");
  EXPECT_EQ(b->getOperation(), expression::operation::symbol);
  EXPECT_EQ(b->extractSymbol_(), U'b');
  EXPECT_EQ(b->extractSymbol(), "b");
  EXPECT_EQ(b->to_u32string(), U"b");
  EXPECT_EQ(b->to_string(), "b");
  auto epsilonu8 = expression::spawnSymbol("");
  auto au8 = expression::spawnSymbol("a");
  auto bu8 = expression::spawnSymbol("b");
  EXPECT_EQ(epsilonu8->getOperation(), expression::operation::symbol);
  EXPECT_EQ(epsilonu8->extractSymbol_(), U'\0');
  EXPECT_EQ(epsilonu8->extractSymbol(), "");
  EXPECT_EQ(epsilonu8->to_u32string(), U"ε");
  EXPECT_EQ(epsilonu8->to_string(), u8"ε");
  EXPECT_EQ(au8->getOperation(), expression::operation::symbol);
  EXPECT_EQ(au8->extractSymbol_(), U'a');
  EXPECT_EQ(au8->extractSymbol(), "a");
  EXPECT_EQ(au8->to_u32string(), U"a");
  EXPECT_EQ(au8->to_string(), "a");
  EXPECT_EQ(bu8->getOperation(), expression::operation::symbol);
  EXPECT_EQ(bu8->extractSymbol_(), U'b');
  EXPECT_EQ(bu8->extractSymbol(), "b");
  EXPECT_EQ(bu8->to_u32string(), U"b");
  EXPECT_EQ(bu8->to_string(), "b");
  auto ab = expression::spawnConcatenation(a, b);
  EXPECT_EQ(ab->getOperation(), expression::operation::concatenation);
  EXPECT_EQ(ab->to_u32string(), U"ab");
  EXPECT_EQ(ab->to_string(), "ab");
  auto epsab = expression::spawnConcatenation(epsilon, ab, false);
  EXPECT_EQ(epsab->getOperation(), expression::operation::concatenation);
  EXPECT_EQ(epsab->to_u32string(), U"εab");
  EXPECT_EQ(epsab->to_string(), u8"εab");
  auto justab = expression::spawnConcatenation(epsilon, ab);
  EXPECT_EQ(justab->getOperation(), expression::operation::concatenation);
  EXPECT_EQ(justab->to_u32string(), U"ab");
  EXPECT_EQ(justab->to_string(), "ab");
  auto aborempty = expression::spawnAlternation(ab, empty, false);
  EXPECT_EQ(aborempty->getOperation(), expression::operation::alternation);
  EXPECT_EQ(aborempty->to_u32string(), U"ab+∅");
  EXPECT_EQ(aborempty->to_string(), u8"ab+∅");
  auto aboreps = expression::spawnAlternation(ab, epsilon);
  EXPECT_EQ(aboreps->getOperation(), expression::operation::alternation);
  EXPECT_EQ(aboreps->to_u32string(), U"ab+ε");
  EXPECT_EQ(aboreps->to_string(), u8"ab+ε");
  auto abor = expression::spawnAlternation(ab, empty);
  EXPECT_EQ(abor->getOperation(), expression::operation::concatenation);
  EXPECT_EQ(abor->to_u32string(), U"ab");
  EXPECT_EQ(abor->to_string(), "ab");
  auto aborepsstar = expression::spawnKleene(aboreps);
  EXPECT_EQ(aborepsstar->getOperation(), expression::operation::kleene);
  EXPECT_EQ(aborepsstar->to_u32string(), U"(ab+ε)*");
  EXPECT_EQ(aborepsstar->to_string(), u8"(ab+ε)*");
  auto aora = expression::spawnAlternation(a, a, false);
  EXPECT_EQ(aora->getOperation(), expression::operation::alternation);
  EXPECT_EQ(aora->to_u32string(), U"a+a");
  EXPECT_EQ(aora->to_string(), "a+a");
  auto justa = expression::spawnAlternation(a, a);
  EXPECT_EQ(justa->getOperation(), expression::operation::symbol);
  EXPECT_EQ(justa->to_u32string(), U"a");
  EXPECT_EQ(justa->to_string(), "a");
}

TEST(ExpressionTest, StringParserTest) {
  using namespace reg;
  expression::exptr re = expression::spawnFromString_(U"a+bε+d∅+∅+ε+c*+ε*+∅*");
  ASSERT_TRUE(re);
  EXPECT_EQ(re->to_u32string(), U"a+bε+d∅+∅+ε+c*+ε*+∅*");
  EXPECT_EQ(re->to_string(), u8"a+bε+d∅+∅+ε+c*+ε*+∅*");
  expression::exptr reu8 = expression::spawnFromString(u8"a+bε+d∅+∅+ε+c*+ε*+∅*");
  ASSERT_TRUE(reu8);
  EXPECT_EQ(reu8->to_u32string(), U"a+bε+d∅+∅+ε+c*+ε*+∅*");
  EXPECT_EQ(reu8->to_string(), u8"a+bε+d∅+∅+ε+c*+ε*+∅*");
  expression::exptr eq = expression::spawnAlternation(
    expression::spawnAlternation(
      expression::spawnSymbol("a"),
      expression::spawnSymbol("b")
    ),
    expression::spawnKleene(
      expression::spawnSymbol("c")
    )
  );
  EXPECT_EQ(*re, *eq);
  EXPECT_EQ(*reu8, *eq);
  re = expression::spawnFromString_(U"a+bε+d∅+∅+ε+c*+ε*+∅*", true);
  EXPECT_TRUE(re);
  EXPECT_EQ(re->to_u32string(), U"a+b+ε+c*+ε+ε");
  EXPECT_EQ(re->to_string(), u8"a+b+ε+c*+ε+ε");
  EXPECT_EQ(*re, *eq);
  reu8 = expression::spawnFromString(u8"a+bε+d∅+∅+ε+c*+ε*+∅*", true);
  EXPECT_TRUE(reu8);
  EXPECT_EQ(reu8->to_u32string(), U"a+b+ε+c*+ε+ε");
  EXPECT_EQ(reu8->to_string(), u8"a+b+ε+c*+ε+ε");
  EXPECT_EQ(*eq, *reu8);
  EXPECT_THROW(expression::spawnFromString_(U"()"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString_(U"(a+b"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString_(U"a+b)"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString_(U"a+"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString_(U"+b"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString_(U"*"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString_(U"+*"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString("()"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString("(a+b"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString("a+b)"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString("a+"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString("+b"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString("*"), std::invalid_argument);
  EXPECT_THROW(expression::spawnFromString("+*"), std::invalid_argument);
  std::string text = u8"ε(λ+|∅|0)";
  reu8 = expression::spawnFromString(text, true);
  EXPECT_EQ(reu8->to_string(), u8"λ");
  expression::E = U'λ'; expression::A = U'|'; expression::N = U'0';
  reu8 = expression::spawnFromString(text, true);
  expression::L = U'<'; expression::R = U'>'; expression::A = U'∨';
  EXPECT_EQ(reu8->to_string(), u8"ε<+∨∅>");
  expression::reset();
  EXPECT_EQ(reu8->to_string(), u8"ε(++∅)");
  reu8 = expression::spawnFromString(u8"🇩🇪");
  EXPECT_EQ(static_cast<nfa const&>(*reu8).getAlphabet(), std::vector<std::string>({"", u8"🇩", u8"🇪"}));
  EXPECT_EQ(findShortestWord(*reu8), u8"🇩🇪");
}

} // namespace
