// Copyright 2017, 2018 Tom Kranz
//
// This file is part of reglibcpp.
//
// reglibcpp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reglibcpp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reglibcpp.  If not, see <https://www.gnu.org/licenses/>.

/// Contains the @ref reg::expression member definitions. @file
#include "expression.h"

using std::make_pair;
using std::make_unique;
using std::pair;
using std::string;
using std::u32string;
using std::unique_ptr;
using std::vector;

#include <algorithm>

#include <array>
using std::array;

#include <bitset>
using std::bitset;

#include "dfa.h"
#include "fabuilder.h"
#include "gnfa.h"
#include "nfa.h"

namespace reg {
expression::exptr expression::empty = expression::exptr(new expression);
std::unordered_map<char32_t, expression::exptr> expression::symbols;

char32_t expression::L =
    U'(';  ///< The symbol used to represent the <b>L</b>eft parenthesis in a
           ///< regular expression.
char32_t expression::R =
    U')';  ///< The symbol used to represent the <b>R</b>ight parenthesis in a
           ///< regular expression.
char32_t expression::K =
    U'*';  ///< The symbol used to represent the <b>K</b>leene star in a regular
           ///< expression.
char32_t expression::A =
    U'+';  ///< The symbol used to represent the <b>A</b>lternation in a regular
           ///< expression.
char32_t expression::E =
    U'ε';  ///< The symbol used to represent the <b>E</b>mpty string in a
           ///< regular expression.
char32_t expression::N =
    U'∅';  ///< The symbol used to represent the <b>N</b>ull/empty set in a
           ///< regular expression.

/// Resets the symbols used for RE operators to their defaults.
void expression::reset() {
  L = U'(';
  R = U')';
  K = U'*';
  A = U'+';
  E = U'ε';
  N = U'∅';
}

/// Gives an RE representing the empty set &empty;.
/**
 * More formally, the RE&apos;s language will be {}.
 * @return `exptr` to the RE representing the empty set &empty;
 */
expression::exptr const& expression::spawnEmptySet() {
  return expression::empty;
}

/// Gives an RE representing the empty string &epsilon;.
/**
 * More formally, the RE&apos;s language will be {&epsilon;}.
 * @return `exptr` to the RE representing the empty string &epsilon;
 */
expression::exptr const& expression::spawnEmptyString() {
  return expression::spawnSymbol_(U'\0');
}

/// Gives an RE representing the given UTF-32-encoded symbol.
/**
 * More formally, the RE&apos;s language will be {&lt;@p symbol&gt;}.
 * @param  symbol the symbol the RE should represent or `&quot;&quot;` for the
 *                [empty string &epsilon;](@ref spawnEmptyString)
 * @return        `exptr` to the RE representing the symbol
 */
expression::exptr const& expression::spawnSymbol_(char32_t symbol) {
  return expression::symbols.emplace(symbol, new expression(symbol))
      .first->second;
}

/// Gives an RE representing the given UTF-32-encoded symbol.
/**
 * This converts @p symbol to UTF-32 and calls @ref spawnSymbol_(char32_t) with
 * the @ref std::u32string&apos;s first `char32_t`. If you don&apos;t want that
 * overhead and already have a `char32_t` on your hands, use that method.
 *
 * More formally, the RE&apos;s language will be {&lt;@p symbol&gt;}.
 * @param  symbol the symbol the RE should represent or `&quot;&quot;` for the
 *                [empty string &epsilon;](@ref spawnEmptyString)
 * @return        `exptr` to the RE representing the symbol
 */
expression::exptr const& expression::spawnSymbol(string const& symbol) {
  char32_t u32Symbol(converter.from_bytes(symbol)[0]);
  return expression::symbols.emplace(u32Symbol, new expression(u32Symbol))
      .first->second;
}

/// Gives an RE representing the concatenation of two given REs.
/**
 * More formally, the RE&apos;s language will be _L_(`lr`) = _L_(`l`) &bull;
 * _L_(`r`).
 * @param  l          `exptr` to the first RE
 * @param  r          `exptr` to the second RE
 * @param  optimized  whether simplifications on the syntax level should be
 *                    applied
 * @param  aggressive whether the simplifications should check the semantic
 *                    level
 * @return            `exptr` to the RE representing the concatenation of `l`
 *                    and `r`
 */
expression::exptr expression::spawnConcatenation(expression::exptr const& l,
                                                 expression::exptr const& r,
                                                 bool optimized,
                                                 bool aggressive) {
  if (optimized) {
    if (l == expression::spawnEmptyString()) {
      return r;
    }
    if (r == expression::spawnEmptyString()) {
      return l;
    }
    if (r == expression::spawnEmptySet() || l == expression::spawnEmptySet()) {
      return expression::spawnEmptySet();
    }
    if (aggressive) {
      if (*l == *expression::spawnEmptyString()) {
        return r;
      }
      if (*r == *expression::spawnEmptyString()) {
        return l;
      }
      if (*r == *expression::spawnEmptySet() ||
          *l == *expression::spawnEmptySet()) {
        return expression::spawnEmptySet();
      }
    }
  }
  return expression::exptr(new expression(l, r, operation::concatenation));
}

/// Gives an RE representing the alternation of two given REs.
/**
 * More formally, the RE&apos;s language will be _L_(`l`+`r`) = _L_(`l`) &cup;
 * _L_(`r`).
 * @param  l          `exptr` to one of the REs
 * @param  r          `exptr` to the other RE
 * @param  optimized  whether simplifications on the syntax level should be
 *                    applied
 * @param  aggressive whether the simplifications should check the semantic
 *                    level
 * @return            `exptr` to the RE representing the alternation of `l` and
 *                    `r`
 */
expression::exptr expression::spawnAlternation(expression::exptr const& l,
                                               expression::exptr const& r,
                                               bool optimized,
                                               bool aggressive) {
  if (optimized) {
    if (l == expression::spawnEmptySet()) {
      return r;
    }
    if (r == expression::spawnEmptySet()) {
      return l;
    }
    if (l == r) {
      return l;
    }
    if (aggressive) {
      if (*l == *expression::spawnEmptySet()) {
        return r;
      }
      if (*r == *expression::spawnEmptySet()) {
        return l;
      }
      if (*l == *r) {
        return l->size() > r->size() ? r : l;
      }
      static dfa empty;
      if (empty == nfa::subtract(*l, *r).buildDfa(true)) {
        return r;
      }
      if (empty == nfa::subtract(*r, *l).buildDfa(true)) {
        return l;
      }
    }
  }
  return expression::exptr(new expression(l, r, operation::alternation));
}

/// Gives an RE representing the Kleene closure of a given RE.
/**
 * More formally, the RE&apos;s language will be _L_(`b`*) = _L_(`b`)*.
 * @param  b          `exptr` to the RE
 * @param  optimized  whether simplifications on the syntax level should be
 *                    applied
 * @param  aggressive whether the simplifications should check the semantic
 *                    level
 * @return            `exptr` to the RE representing the Kleene closure of `l`
 */
expression::exptr expression::spawnKleene(expression::exptr const& b,
                                          bool optimized, bool aggressive) {
  if (optimized) {
    if (b == expression::spawnEmptySet() ||
        b == expression::spawnEmptyString()) {
      return expression::spawnEmptyString();
    }
    if (aggressive) {
      if (*b == *expression::spawnEmptySet() ||
          *b == *expression::spawnEmptyString()) {
        return expression::spawnEmptyString();
      }
      if (*b == expression(b)) {
        return b;
      }
    }
  }
  return expression::exptr(new expression(b));
}

/// Reports the size of this RE&apos;s tree representation.
/**
 * In this context, an RE&apos;s size will be defined recursively as follows:
 * - &empty;`.size()` = 1
 * - &epsilon;`.size()` = 1
 * - &lt;`symbol`&gt;`.size()` = 1
 * - `(l+r).size()` = 1 `+ l.size() + r.size()`
 * - `(lr).size()` = 1 `+ l.size() + r.size()`
 * - `(b*).size()` = 1 `+ b.size()`
 * @return a measure of how many subexpressions this RE consists of
 */
size_t expression::size() const {
  size_t s(1);
  for (exptr const& re : *this) {
    s += re->size();
  }
  return s;
}

/// Reports this RE&apos;s function.
/**
 * Note that the [empty string](@ref spawnEmptyString)&apos;s function is
 * technically that of a [symbol](@ref spawnSymbol).
 * @return the @ref expression::operation best describing this RE&apos;s purpose
 */
expression::operation expression::getOperation() const { return op; }

#ifndef DOXYGEN_SHOULD_SKIP_THIS
expression::operator nfa const&() const {
  if (!acceptingNfa) {
    acceptingNfa = make_unique<nfa const>(gnfa(*this).splitAllTransitions());
  }
  return *acceptingNfa;
}
#endif  // DOXYGEN_SHOULD_SKIP_THIS

/// Checks whether this RE describes the same regular language as another
/// object.
/**
 * @return `true` if this RE&apos;s language is exactly the same as the other
 *         object&apos;s, `false` else
 */
bool expression::operator==(nfa const& other) const {
  return other.operator==(*this);
}

/// Checks whether this RE describes a different regular language than another
/// object.
/**
 * @return `false` if this RE&apos;s language is exactly the same as the other
 *         object&apos;s, `true` else
 */
bool expression::operator!=(nfa const& other) const {
  return !operator==(other);
}

/// Reports this [symbol expression](@ref spawnSymbol)&apos;s UTF-32-encoded
/// symbol.
/**
 * @return the `char32_t` encoded within this
 *         [symbol expression](@ref spawnSymbol), <code>U'\0'</code> for an
 *         empty string
 * @throws std::logic_error if this expression&apos;s
 *         [purpose](@ref getOperation) is not that of a symbol
 */
char32_t expression::extractSymbol_() const {
  auto it = std::find_if(expression::symbols.begin(), expression::symbols.end(),
                         [&](pair<char32_t, exptr> const& entry) -> bool {
                           return entry.second.get() == this;
                         });
  if (it == expression::symbols.end()) {
    throw std::logic_error(
        "This RE does not seem to be a valid symbol expression.");
  }
  return it->first;
}

/// Reports this [symbol expression](@ref spawnSymbol)&apos;s UTF-8-encoded
/// symbol.
/**
 * @return the character encoded within this
 *         [symbol expression](@ref spawnSymbol), `""` for an empty string
 * @throws std::logic_error if this expression&apos;s
 *         [purpose](@ref getOperation) is not that of a symbol
 */
string expression::extractSymbol() const {
  char32_t symbol(extractSymbol_());
  return converter.to_bytes(u32string(!!symbol, symbol));
}

/// Describes this RE in UTF-32-encoded human-readable form.
u32string expression::to_u32string() const {
  switch (op) {
    case operation::alternation:
      return subExpressions[0]->to_u32string() + A +
             subExpressions[1]->to_u32string();
    case operation::concatenation: {
      u32string concat;
      if (subExpressions[0]->op >= operation::alternation) {
        concat.append(L + subExpressions[0]->to_u32string() + R);
      } else {
        concat.append(subExpressions[0]->to_u32string());
      }
      if (subExpressions[1]->op >= operation::alternation) {
        concat.append(L + subExpressions[1]->to_u32string() + R);
      } else {
        concat.append(subExpressions[1]->to_u32string());
      }
      return concat;
    }
    case operation::kleene: {
      if (subExpressions[0]->op >= operation::concatenation) {
        return (L + subExpressions[0]->to_u32string() + R) + K;
      } else {
        return subExpressions[0]->to_u32string() + K;
      }
    }
    case operation::symbol: {
      char32_t symbol = extractSymbol_();
      return u32string(1, symbol + (!symbol * E));
    }
    case operation::empty:
      return u32string(1, N);
    default:
      return u32string();
  }
}

/// Describes this RE in UTF-8-encoded human-readable form.
string expression::to_string() const {
  return converter.to_bytes(to_u32string());
}

/// Returns an `iterator` pointing to this RE&apos;s first subexpression.
vector<expression::exptr>::const_iterator expression::begin() const {
  return subExpressions.cbegin();
}

/// Returns an `iterator` pointing behind this RE&apos;s last subexpression.
vector<expression::exptr>::const_iterator expression::end() const {
  return subExpressions.cend();
}

/// Parses regular expressions.
/**
 * Recognizes REs generated by the following context-free grammar, where [P]
 * (@ref parser::token::P) stands for the __alternative__ symbol
 * ([“+” or “|”](@ref A)), [L](@ref parser::token::L) for a left  parenthesis
 * ([“(”](@ref L)), [R](@ref parser::token::R) for a right parenthesis
 * ([“)”](@ref R)) and [S](@ref parser::token::S) for the __Kleene star__
 * ([“*”](@ref K)). [Σ](@ref parser::token::Σ) is a placeholder for any symbol
 * that makes up the language described by the RE, the empty word
 * ([“&epsilon;” or “&lambda;”](@ref E)) or the empty set ([“&empty;”](@ref N)).
 *
 *     GRE=(
 *            {A, B, C, K, E, F},
 *            {Σ, P, L, R, S}, A,
 *            {
 *              (A,C), (A,AB),
 *              (B,PC),
 *              (C,K), (C,CK),
 *              (K,E), (K,ES),
 *              (E,Σ), (E,LF),
 *              (F,AR)
 *            }
 *         )
 *
 * This parser is based on the article <i>To CNF or not to CNF? An Efficient Yet
 * Presentable Version of the CYK Algorithm</i> by Martin Lange and Hans
 * Lei&szlig;, published in <i>[informatica didactica No. 8]
 * (https://www.informaticadidactica.de/index.php?page=issue-no-8---2008---2010)
 * </i>. Conventions will be based on this article.
 */
struct expression::parser {
  /// Tokens the grammar deals with.
  enum struct token : unsigned char {
    A,   ///< Beginning of an alternation expression.
    B,   ///< Second part of an alternation expression.
    C,   ///< A concatenation expression.
    K,   ///< Kleene expression.
    E,   ///< Beginning of a new subexpression.
    F,   ///< Second part of a new subexpression.
    Σ,   ///< A symbol expression.
    P,   ///< An alternation symbol.
    L,   ///< A left parenthesis.
    R,   ///< A right parenthesis.
    S,   ///< A Kleene star symbol.
    END  ///< Number of elements in this enumeration, __NOT AN ACTUAL TOKEN!__
  };

/// Gives casting to base type back to scoped enums, as God intended.
#define TOKEN(T) static_cast<unsigned char>(reg::expression::parser::token::T)

  /// Tokens don&apos;t usually come alone.
  typedef bitset<TOKEN(END)> tokens;

  static array<token, TOKEN(END)> const inverseUnitGraph;
  ///< Maps symbols that may be derived in-place to their predecessing symbols.
  /**< See **Definition 2** and **Lemma 4** in the article. */

  static array<array<tokens, TOKEN(END)>, TOKEN(END)> const inverseBinaryRules;
  ///< Maps pairs of symbols to the symbols that derive them.

  /// Constructs the reflexive-transitive closure of the inverse unit relation
  /// for a given set of symbols.
  /**
   * See **Lemma 5** in the article.
   *
   * @param m the set of symbols that may be derived in-place
   * @return  the set of symbols that may be predecessors to any of the symbols
   *          in m
   */
  static tokens getUClosure(tokens const& m) {
    tokens closure;
    for (unsigned char c(0); c < TOKEN(END); c++) {
      if (m[c]) {
        for (token s(static_cast<token>(c)); s != token::END;
             s = inverseUnitGraph[static_cast<unsigned char>(s)]) {
          closure.set(static_cast<unsigned char>(s));
        }
      }
    }  // Going through the tokens in m.
    return closure;
  }

  /// Checks if a token could derive a pair of tokens from two other entries.
  /**
   * @param symbol the original token
   * @param left   the set of tokens allowed for the first half of the result
   *               pair
   * @param right  the set of tokens allowed for the second half of the result
   *               pair
   * @return       `true` if the original token may lead to any such pair,
   *               `false` else
   */
  static bool canDerive(token symbol, tokens const& left, tokens const& right) {
    tokens leftClosure(getUClosure(left));
    tokens rightClosure(getUClosure(right));
    for (unsigned char li(0); li < TOKEN(END); li++) {
      if (leftClosure[li]) {
        for (unsigned char ri(0); ri < TOKEN(END); ri++) {
          if (rightClosure[ri]) {
            for (unsigned char ci(0); ci < TOKEN(END); ci++) {
              if (inverseBinaryRules[li][ri][ci]) {
                for (unsigned char successor(ci); successor != TOKEN(END);
                     successor = static_cast<unsigned char>(
                         inverseUnitGraph[successor])) {
                  if (static_cast<unsigned char>(symbol) == successor) {
                    return true;
                  }
                }
              }
            }
          }
        }
      }
    }
    return false;
  }

  /// Fills a table entry.
  /**
   * The entry will be filled with the set of symbols deriving the subsentence
   * beginning at the diagonal entry in the same row and ending at the diagonal
   * entry in the same column.
   *
   * @param row   the row of the entry to fill
   * @param diag  the diagonal of the entry to fill
   * @param table the table to operate on
   */
  static void compileTableEntry(size_t row, size_t diag,
                                vector<vector<tokens>>& table) {
    for (size_t i(0); i < diag; i++) {
      tokens first(getUClosure(table[row][i]));
      for (unsigned char fi(0); fi < first.size(); fi++) {
        if (first[fi]) {
          tokens second(getUClosure(table[row + 1 + i][diag - i - 1]));
          for (unsigned char si(0); si < second.size(); si++) {
            if (second[si]) {
              table[row][diag] |= inverseBinaryRules[fi][si];
            }
          }  // Going through the tokens in second.
        }
      }  // Going through the tokens in first.
    }
  }

  vector<char32_t> symbolMapping;  ///< Stores the actual symbols encountered in
                                   ///< the RE while parsing.
  size_t mutable symbolMappingIndex;  ///< Index for when symbols have to be
                                      ///< extracted from the mapping.
  vector<vector<tokens>>
      table;  ///< The table of sets of symbols that derive a subsentence.
  bool badRegularExpression =
      false;  ///< Signifies that the RE used to [initialize](@ref parser())
              ///< this object was invalid.

  /// Initializes with a string to parse.
  parser(u32string const& re) {
    if (re.empty()) {
      badRegularExpression = true;
      return;
    }
    symbolMappingIndex = 0;
    size_t numberOfTokens(re.length());
    symbolMapping.reserve(numberOfTokens);
    table.reserve(numberOfTokens);
    size_t row(0);
    for (char32_t symbol : re) {
      table.push_back(vector<tokens>(numberOfTokens - row, tokens()));
      if (symbol == L) {
        table[row][0].set(TOKEN(L));
      } else if (symbol == R) {
        table[row][0].set(TOKEN(R));
      } else if (symbol == A) {
        table[row][0].set(TOKEN(P));
      } else if (symbol == K) {
        table[row][0].set(TOKEN(S));
      } else {
        table[row][0].set(TOKEN(Σ));
        symbolMapping.push_back(symbol);
      }
      row++;
    }
    symbolMapping.shrink_to_fit();
    for (size_t diag(1); diag < numberOfTokens; diag++) {
      for (size_t row(0); row < numberOfTokens - diag; row++) {
        compileTableEntry(row, diag, table);
      }
    }
    if (!getUClosure(table[0][table[0].size() - 1])[TOKEN(A)]) {
      badRegularExpression = true;
    }
  }

  /// Represents the @ref table entries as binary trees.
  struct tree {
    parser const* p;  ///< Points to the parser this tree belongs to.
    token symbol;     ///< This tree's root symbol.
    pair<unique_ptr<tree>, unique_ptr<tree>> children;
    ///< Trees with the symbols of the entry&apos;s derived pair as root.

    /// Finds the child trees that can be derived from a given entry.
    /**
     * @param symbol the entry&apos;s symbol
     * @param row    the row of the entry
     * @param diag   the diagonal of the entry
     * @param p      points to this tree&apos;s owning parser
     * @return       pair of trees, both of which can be derived from the entry
     *               (may both be {@code nullptr} if the entry is nullable)
     */
    pair<unique_ptr<tree>, unique_ptr<tree>> findNextPair(token symbol,
                                                          size_t row,
                                                          size_t diag,
                                                          parser const* p) {
      for (size_t i = 0; i < diag; i++) {
        size_t leftDiag = diag - i - 1;
        size_t rightDiag = i;
        size_t rightRow = diag + row - rightDiag;
        if (canDerive(symbol, p->table[row][leftDiag],
                      p->table[rightRow][rightDiag])) {
          return make_pair(make_unique<tree>(row, leftDiag, p),
                           make_unique<tree>(rightRow, rightDiag, p));
        }
      }
      return make_pair(unique_ptr<tree>(), unique_ptr<tree>());
    }

    /// Initializes a tree with a given table entry as root.
    /**
     * @param row  the row of the entry
     * @param diag the diagonal of the entry
     * @param p    pointer to the calling parser
     */
    tree(size_t row, size_t diag, parser const* p)
        : p(p),
          symbol([&]() -> token {
            for (unsigned char i(TOKEN(END)); i > 0; i--) {
              if (p->table[row][diag][i - 1]) {
                return static_cast<token>(i - 1);
              }
            }
            return token::END;
          }()),
          children(findNextPair(symbol, row, diag, p)) {}

    /// Gives the RE encoded in this tree.
    exptr operator()(bool optimized, bool aggressive) {
      switch (symbol) {
        case token::A:
          return spawnAlternation((*children.first)(optimized, aggressive),
                                  (*children.second)(optimized, aggressive),
                                  optimized, aggressive);
        case token::B:
          return (*children.second)(optimized, aggressive);
        case token::C:
          return spawnConcatenation((*children.first)(optimized, aggressive),
                                    (*children.second)(optimized, aggressive),
                                    optimized, aggressive);
        case token::K:
          return spawnKleene((*children.first)(optimized, aggressive),
                             optimized, aggressive);
        case token::E:
          return (*children.second)(optimized, aggressive);
        case token::F:
          return (*children.first)(optimized, aggressive);
        case token::Σ: {
          char32_t symbol = p->symbolMapping[p->symbolMappingIndex++];
          if (p->symbolMappingIndex >= p->symbolMapping.size()) {
            p->symbolMappingIndex = 0;
          }
          if (symbol == E) {
            return spawnEmptyString();
          } else if (symbol == N) {
            return spawnEmptySet();
          } else {
            return spawnSymbol_(symbol);
          }
        }
        default:
          return exptr();
      }
    }
  };

  /// Gives the RE resulting from parsing.
  /**
   * @return `exptr` to the RE represented by the initialization string
   * @throws std::bad_function_call if the regular expression string was
   *         malformed
   */
  exptr operator()(bool optimized, bool aggressive) {
    if (badRegularExpression) {
      throw std::bad_function_call();
    }
    return tree(0, table.size() - 1, this)(optimized, aggressive);
  }
};

auto const expression::parser::inverseUnitGraph =
    []() -> array<expression::parser::token, TOKEN(END)> {
  array<token, TOKEN(END)> graph;
  graph.fill(token::END);
  graph[TOKEN(Σ)] = token::E;
  graph[TOKEN(E)] = token::K;
  graph[TOKEN(K)] = token::C;
  graph[TOKEN(C)] = token::A;
  return graph;
}();

auto const expression::parser::inverseBinaryRules =
    []() -> array<array<expression::parser::tokens, TOKEN(END)>, TOKEN(END)> {
  array<tokens, TOKEN(END)> noPredecessor;
  noPredecessor.fill(tokens());
  array<array<tokens, TOKEN(END)>, TOKEN(END)> rules;
  rules.fill(noPredecessor);
  rules[TOKEN(A)][TOKEN(B)].set(TOKEN(A));
  rules[TOKEN(P)][TOKEN(C)].set(TOKEN(B));
  rules[TOKEN(C)][TOKEN(K)].set(TOKEN(C));
  rules[TOKEN(E)][TOKEN(S)].set(TOKEN(K));
  rules[TOKEN(L)][TOKEN(F)].set(TOKEN(E));
  rules[TOKEN(A)][TOKEN(R)].set(TOKEN(F));
  return rules;
}();
#undef TOKEN

/// Gives an RE encoded in a given UTF-32 string.
/**
 * @param  u32re      the RE in text form
 * @param  optimized  whether simplifications on the syntax level should be
 *                    applied
 * @param  aggressive whether the simplifications should check the semantic
 *                    level
 * @return            `exptr` to the RE represented by the given string
 * @throws std::invalid_argument if the @p u32re string is malformed
 */
expression::exptr expression::spawnFromString_(std::u32string const& u32re,
                                               bool optimized,
                                               bool aggressive) {
  parser stringParser(u32re);
  try {
    return stringParser(optimized, aggressive);
  } catch (std::bad_function_call e) {
    throw std::invalid_argument("Malformed regular expression.");
  }
}

/// Gives an RE encoded in a given UTF-8 string.
/**
 * This converts @p re to UTF-32 and calls
 * @ref spawnFromString_(std::u32string const&,bool,bool).
 * If you don&apos;t want that overhead and already have a @ref std::u32string
 * on your hands, use that method.
 *
 * @param  re         the RE in text form
 * @param  optimized  whether simplifications on the syntax level should be
 *                    applied
 * @param  aggressive whether the simplifications should check the semantic
 *                    level
 * @return            `exptr` to the RE represented by the given string
 * @throws std::invalid_argument if the @p re string is malformed
 */
expression::exptr expression::spawnFromString(string const& re, bool optimized,
                                              bool aggressive) {
  return spawnFromString_(converter.from_bytes(re), optimized, aggressive);
}

expression::expression() : op(operation::empty) {}
expression::expression(char32_t symbol) : op(operation::symbol) {}
expression::expression(exptr const& l, exptr const& r, operation op)
    : subExpressions({l, r}), op(op) {}
expression::expression(exptr const& b)
    : subExpressions({b}), op(operation::kleene) {}
}  // namespace reg
